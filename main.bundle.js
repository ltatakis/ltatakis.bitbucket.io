webpackJsonp([1],{

/***/ "../../../../../src async recursive":
/***/ (function(module, exports) {

function webpackEmptyContext(req) {
	throw new Error("Cannot find module '" + req + "'.");
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = "../../../../../src async recursive";

/***/ }),

/***/ "../../../../../src/app/Reservation.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Reservation; });
var Reservation = (function () {
    function Reservation() {
    }
    return Reservation;
}());

//# sourceMappingURL=Reservation.js.map

/***/ }),

/***/ "../../../../../src/app/app-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__search_search_component__ = __webpack_require__("../../../../../src/app/search/search.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__contact_contact_component__ = __webpack_require__("../../../../../src/app/contact/contact.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__crete_crete_component__ = __webpack_require__("../../../../../src/app/crete/crete.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__terms_terms_component__ = __webpack_require__("../../../../../src/app/terms/terms.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__privacy_privacy_component__ = __webpack_require__("../../../../../src/app/privacy/privacy.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__book_book_component__ = __webpack_require__("../../../../../src/app/book/book.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__confirmation_confirmation_component__ = __webpack_require__("../../../../../src/app/confirmation/confirmation.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__not_found404_not_found404_component__ = __webpack_require__("../../../../../src/app/not-found404/not-found404.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__main_main_component__ = __webpack_require__("../../../../../src/app/main/main.component.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppRoutingModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











var routes = [
    { path: '', redirectTo: '/main', pathMatch: 'full' },
    { path: 'main', component: __WEBPACK_IMPORTED_MODULE_10__main_main_component__["a" /* MainComponent */] },
    { path: 'search', component: __WEBPACK_IMPORTED_MODULE_2__search_search_component__["a" /* SearchComponent */] },
    { path: 'crete', component: __WEBPACK_IMPORTED_MODULE_4__crete_crete_component__["a" /* CreteComponent */] },
    { path: 'contact', component: __WEBPACK_IMPORTED_MODULE_3__contact_contact_component__["a" /* ContactComponent */] },
    { path: 'terms', component: __WEBPACK_IMPORTED_MODULE_5__terms_terms_component__["a" /* TermsComponent */] },
    { path: 'privacy', component: __WEBPACK_IMPORTED_MODULE_6__privacy_privacy_component__["a" /* PrivacyComponent */] },
    { path: 'book', component: __WEBPACK_IMPORTED_MODULE_7__book_book_component__["a" /* BookComponent */] },
    { path: 'confirm', component: __WEBPACK_IMPORTED_MODULE_8__confirmation_confirmation_component__["a" /* ConfirmationComponent */] },
    { path: '**', component: __WEBPACK_IMPORTED_MODULE_9__not_found404_not_found404_component__["a" /* NotFound404Component */] } // Maybe this should be 404.
];
var AppRoutingModule = (function () {
    function AppRoutingModule() {
    }
    return AppRoutingModule;
}());
AppRoutingModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["b" /* NgModule */])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* RouterModule */].forRoot(routes, { enableTracing: true } // <-- debugging purposes only
            )
        ],
        exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* RouterModule */]]
    })
], AppRoutingModule);

//# sourceMappingURL=app-routing.module.js.map

/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\n<app-header></app-header>\n\n\n<router-outlet></router-outlet>\n\n\n<app-footer> </app-footer>\n\n\n"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = (function () {
    function AppComponent() {
    }
    return AppComponent;
}());
AppComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_14" /* Component */])({
        selector: 'app-root',
        template: __webpack_require__("../../../../../src/app/app.component.html"),
        styles: [__webpack_require__("../../../../../src/app/app.component.css")]
    })
], AppComponent);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__ = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__footer_footer_component__ = __webpack_require__("../../../../../src/app/footer/footer.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__header_header_component__ = __webpack_require__("../../../../../src/app/header/header.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__main_main_component__ = __webpack_require__("../../../../../src/app/main/main.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__search_search_component__ = __webpack_require__("../../../../../src/app/search/search.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_routing_module__ = __webpack_require__("../../../../../src/app/app-routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__contact_contact_component__ = __webpack_require__("../../../../../src/app/contact/contact.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__crete_crete_component__ = __webpack_require__("../../../../../src/app/crete/crete.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__terms_terms_component__ = __webpack_require__("../../../../../src/app/terms/terms.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__privacy_privacy_component__ = __webpack_require__("../../../../../src/app/privacy/privacy.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__book_book_component__ = __webpack_require__("../../../../../src/app/book/book.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__confirmation_confirmation_component__ = __webpack_require__("../../../../../src/app/confirmation/confirmation.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__not_found404_not_found404_component__ = __webpack_require__("../../../../../src/app/not-found404/not-found404.component.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

















var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["b" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_4__footer_footer_component__["a" /* FooterComponent */],
            __WEBPACK_IMPORTED_MODULE_5__header_header_component__["a" /* HeaderComponent */],
            __WEBPACK_IMPORTED_MODULE_6__main_main_component__["a" /* MainComponent */],
            __WEBPACK_IMPORTED_MODULE_7__search_search_component__["a" /* SearchComponent */],
            __WEBPACK_IMPORTED_MODULE_9__contact_contact_component__["a" /* ContactComponent */],
            __WEBPACK_IMPORTED_MODULE_10__crete_crete_component__["a" /* CreteComponent */],
            __WEBPACK_IMPORTED_MODULE_11__terms_terms_component__["a" /* TermsComponent */],
            __WEBPACK_IMPORTED_MODULE_12__privacy_privacy_component__["a" /* PrivacyComponent */],
            __WEBPACK_IMPORTED_MODULE_13__book_book_component__["a" /* BookComponent */],
            __WEBPACK_IMPORTED_MODULE_14__confirmation_confirmation_component__["a" /* ConfirmationComponent */],
            __WEBPACK_IMPORTED_MODULE_16__not_found404_not_found404_component__["a" /* NotFound404Component */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__["a" /* NgbModule */],
            __WEBPACK_IMPORTED_MODULE_8__app_routing_module__["a" /* AppRoutingModule */],
            __WEBPACK_IMPORTED_MODULE_15__angular_forms__["a" /* FormsModule */]
        ],
        providers: [],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* AppComponent */]]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ "../../../../../src/app/book/book.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".bookingContainer {\n  background-color: aliceblue;\n  height: 4%;\n  vertical-align: middle;\n  text-align: center; /* FF1+ */ /* Saf3-4 */\n  border-radius: 12px; /* Opera 10.5, IE 9, Saf5, Chrome */\n}\n\n.iconsInfo span {\n  font-size: 18px;\n  line-height: 18px;\n  width: 80%;\n  font-family: \"Helvetica Neue\", Helvetica, Arial, sans-serif;\n  display: block;\n  color: black;\n  /*background: url(\"../img/features-bg-14.png\") no-repeat transparent;*/\n  /*padding: 0px 0px 0px 27px;*/\n  margin: 2px 0px;\n  /*border-bottom: 1px solid #ddd;*/\n  font-weight: 300;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/book/book.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container mt-4 mb-4 col-10 bookingContainer\" >\n  <div class=\"row\">\n  <div class=\"col-sm-7 offset-sm-1 col-xs-12\">\n    <form name=\"myForm\">\n      <div class=\" pickup  col-xs-12 col-sm-10\" >\n        <h3 style=\"margin-left: 0;\">Contact details</h3>\n\n        <div class=\"row form-group\">\n          <label for=\"inputName\" class=\"col-sm-4 col-sm-3 col-form-label\">First Name*: </label>\n          <div class=\"col-sm-8 col-xs-6\">\n            <input type=\"text\"  class=\"form-control \" name=\"inputName\" id=\"inputName\" placeholder=\"Given name\"  ng-model=\"general.givenName\" required >\n          </div>\n        </div>\n        <div class=\" row form-group \">\n          <label for=\"lastName\" class=\"col-sm-4 col-form-label\">Last Name*: </label>\n          <div class=\"col-sm-8\">\n            <input itemprop=\"familyName\"  type=\"text\" class=\"form-control booking\" id=\"lastName\" name=\"lastName\" placeholder=\"Last name\" ng-model=\"general.lastName\" required>\n          </div>\n        </div>\n        <div class=\"row form-group \">\n          <label for=\"email\" class=\"col-sm-4 col-form-label\">Email*: </label>\n          <div class=\"col-sm-8\">\n            <input itemprop=\"email\"   type=\"email\" class=\"form-control booking \" id=\"email\" name=\"email\"  ng-model=\"general.email\" required>\n          </div>\n        </div>\n\n\n        <div class=\"row form-group \">\n          <label for=\"dob\" class=\"col-sm-4 col-form-label\">Date of birth*: </label>\n          <div class=\"col-sm-8\">\n            <input itemprop=\"birthDate\"  type=\"text\" class=\"form-control booking \" name=\"dob\" id=\"dob\" placeholder=\"dd/mm/yyyy\" ng-model=\"general.dob\" required>\n          </div>\n        </div>\n\n        <div class=\"row form-group \">\n          <label for=\"mobile\" class=\"col-sm-4 col-form-label\">Mobile Phone*: </label>\n          <div class=\"col-sm-8\">\n            <input itemprop=\"telephone\" type=\"text\"  class=\"form-control booking \" name=\"mobile\" id=\"mobile\" placeholder=\"\" ng-model=\"general.mobile\"  required>\n          </div>\n        </div>\n\n        <p > We need some additional information regarding car delivery:</p>\n        <div class=\"row form-group \">\n          <label for=\"extrapickup\" class=\"extramore\" class=\"col-sm-4 col-form-label\"\n                 original-title=\"Please enter your flight number. We will&lt;br&gt;then\n                          have the relevant info in case your&lt;br&gt;flight is delayed or cancelled\">\n            Flight Number:</label>\n          <div class=\"col-sm-8\">\n            <input type=\"text\" class=\"form-control\" id=\"extrapickup\" placeholder=\"\" ng-model=\"general.flight\">\n          </div>\n        </div>\n        <div class=\"row form-group \">\n          <label for=\"extrapickup\" class=\"extramore\" class=\"col-sm-4 col-form-label\" original-title=\"Please enter the hotel you will be staying at.\">\n            Hotel:</label>\n          <div class=\"col-sm-8\">\n            <input type=\"text\" class=\"form-control\" id=\"hotel\" placeholder=\"\" ng-model=\"general.hotel\">\n          </div>\n        </div>\n\n        <div class=\"row \">\n          <p>GPS Navigator costs &pound;5 per day. The cost is paid at location.</p>\n          <label for=\"gps\" class=\"col-sm-4 col-form-label\">GPS Navigator</label>\n          <div class=\"col-sm-8\">\n            <select name=\"gps\" class=\" col-4 selectArea\" id=\"gps\" >\n              <option value=\"0\" selected=\"selected\">No</option>\n              <option value=\"1\">Yes &nbsp; (5 &pound; / day)</option>\n            </select>\n          </div>\n        </div>\n        <div class=\"row form-group \">\n          <h3 style=\"margin-left: 0px;\" >Child Seats - FREE of charge!</h3>\n        </div>\n        <div class=\"row form-group \">\n          <label for=\"infant\" class=\"col-sm-4 col-form-label\">Infant seat (0-1 y.o.)</label>\n          <div class=\"col-sm-8\">\n            <select name=\"infant\"   class=\" selectArea \" id=\"infant\" original-title=\"\">\n\n              <option value=\"0\" selected=\"selected\">0</option>\n              <option value=\"1\">1</option>\n              <option value=\"2\">2</option>\n              <option value=\"3\">3</option>\n              <option value=\"4\">4</option>\n            </select>\n          </div>\n        </div>\n        <div class=\"row form-group \">\n          <label for=\"babyseat\" class=\"col-sm-4 col-form-label\" >Baby seat (1-3 y.o.)</label>\n          <div class=\"col-sm-8\">\n            <select name=\"babyseat\" class=\" selectArea\"\n                    id=\"babyseat\" original-title=\"\">\n              <option value=\"0\" selected=\"selected\">0</option>\n              <option value=\"1\">1</option>\n              <option value=\"2\">2</option>\n              <option value=\"3\">3</option>\n              <option value=\"4\">4</option>\n            </select>\n          </div>\n        </div>\n        <div class=\"row form-group \">\n          <label for=\"booster\"   class=\"col-sm-4 col-form-label\">Toddler seat (4-6 y.o.)</label>\n          <div class=\"col-sm-8\">\n            <select name=\"booster\" ng-model=\"general.booster\"\n                    class=\"selectArea\"\n                    id=\"booster\"\n                    original-title=\"\">\n              <option value=\"0\" selected=\"selected\">0</option>\n              <option value=\"1\">1</option>\n              <option value=\"2\">2</option>\n              <option value=\"3\">3</option>\n              <option value=\"4\">4</option>\n            </select>\n          </div>\n        </div>\n        <div class=\"row form-group \">\n          <label>\n            I agree to the  <a style=\"cursor:pointer;color:blue;\"  target=\"_blank\"  href=\"\">\n            terms and conditions</a>\n            <input type=\"checkbox\" value=\"false\">\n            I do</label>\n        </div>\n\n        <div class=\"row form-group ml-4\">\n          <button type=\"submit\"  class=\"btn btn-primary\" routerLink=\"/confirm\" routerLinkActive=\"active\" rel=\"nofollow\" style=\"width: 80%;\" >\n            Confirm booking\n          </button>\n        </div>\n      </div>\n\n    </form>\n  </div>\n  <div class=\"col-sm-3 stick-top\">\n\n    <div class=\"container\">\n      <h2 style=\"text-decoration:underline\" >Reservation info</h2>\n      <div class=\"row\">\n        <div class=\"col-12\">\n           <span> {{ car.model }}</span>\n        </div>\n        <div class=\"col-12\">\n          <img style=\"width:90%; height: 150px; margin-left: 1%\" src=\"assets/{{car.imageUrl}}\">\n        </div>\n        <div class=\"col-12\">\n        <p class=\"iconsInfo carItemsBookings features\">\n          <span class=\"motor\">{{ car.motor }}cc</span>\n          <span class=\"doors\">{{ car.doors }} Doors</span>\n          <span class=\"seats\">{{ car.seats }} Seats</span>\n          <span class=\"airco\">{{ car.ac }}</span>\n          <span class=\"trans\">{{car.type}} Gearbox</span>\n          <span class=\"bags\" style=\"border-bottom:none;\">{{ car.luggage }} luggages</span>\n        </p>\n        </div>\n      </div>\n      <div class=\"row\">\n        <h2>  Total Price: &pound; 1000</h2>\n      </div>\n      <div class=\"row\">\n        <p>\n          <span ><h4>Pick up:</h4></span>\n          <span >  <b>  {{reservation.datePickUp}} </b> from <b itemprop=\"pickupLocation\">{{reservation.pickUpFrom.name}} </b>  at <b itemprop=\"pickupTime\">{{ reservation.pickUpTime }}</b> </span>\n          <br>\n          <span>   <h4>Return:</h4></span>\n          <span > <b> {{ reservation.dateReturn }} </b>from <b itemprop=\"dropoffLocation\">{{reservation.returnTo.name}}</b> at<b itemprop=\"dropoffTime\"> {{ reservation.returnTime }}</b> </span>\n        </p>\n      </div>\n\n    </div>\n\n    <div class=\"row justify-content-center\" style=\"margin-top: 1%;\">\n      <h3> Included in Price</h3>\n      <span >Final price &amp; no extra insurances </span>\n      <span>No booking fee</span>\n      <span>Theft Cover with<b> no excess</b>.<br></span>\n      <span>Full Insurance (Collision Damage Waiver) with<b> no excess</b></span>\n      <span>Insurance for wheels,undeside of car, glasses & mirrors with<b> no excess</b></span>\n      <span>Public Liability Insurance</span>\n      <span>Personal Accident Insurance</span>\n      <span>Unlimited kilometers</span>\n      <span>23% VAT Taxes</span>\n      <span>Fair Fuel Policy</span>\n      <span>24h Road Assistance all over Crete</span>\n      <span>Free Baby or Booster seat</span>\n      <span >Road Map of Crete</span>\n    </div>\n  </div>\n  </div>\n</div>\n\n"

/***/ }),

/***/ "../../../../../src/app/book/book.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__cars_service__ = __webpack_require__("../../../../../src/app/cars.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_switchMap__ = __webpack_require__("../../../../rxjs/add/operator/switchMap.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_switchMap___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_switchMap__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__Reservation__ = __webpack_require__("../../../../../src/app/Reservation.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__locations_service__ = __webpack_require__("../../../../../src/app/locations.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BookComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var BookComponent = (function () {
    function BookComponent(carsService, locationService, route) {
        this.carsService = carsService;
        this.locationService = locationService;
        this.route = route;
        this.reservation = new __WEBPACK_IMPORTED_MODULE_4__Reservation__["a" /* Reservation */]();
    }
    BookComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.paramMap.switchMap(function (params) { return _this.carsService.getCar(params.get('id')); })
            .subscribe(function (car) { return _this.car = car; });
        this.route.paramMap.subscribe(function (params) { return _this.setReservation(params.get('dtr'), params.get('dpu'), params.get('pl'), params.get('rl'), params.get('pT'), params.get('rT')); });
    };
    BookComponent.prototype.setReservation = function (dateReturn, datePickUp, pickUpLocID, returnToLocId, pickUpTime, returnTime) {
        this.reservation.dateReturn = dateReturn;
        this.reservation.datePickUp = datePickUp;
        this.reservation.pickUpFrom = this.locationService.getLocation(pickUpLocID);
        this.reservation.returnTo = this.locationService.getLocation(returnToLocId);
        this.reservation.pickUpTime = pickUpTime;
        this.reservation.returnTime = returnTime;
    };
    return BookComponent;
}());
BookComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_14" /* Component */])({
        selector: 'app-book',
        template: __webpack_require__("../../../../../src/app/book/book.component.html"),
        styles: [__webpack_require__("../../../../../src/app/book/book.component.css")],
        providers: [__WEBPACK_IMPORTED_MODULE_2__cars_service__["a" /* CarsService */], __WEBPACK_IMPORTED_MODULE_5__locations_service__["a" /* LocationsService */]]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__cars_service__["a" /* CarsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__cars_service__["a" /* CarsService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_5__locations_service__["a" /* LocationsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__locations_service__["a" /* LocationsService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */]) === "function" && _c || Object])
], BookComponent);

var _a, _b, _c;
//# sourceMappingURL=book.component.js.map

/***/ }),

/***/ "../../../../../src/app/cars.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__mock_cars__ = __webpack_require__("../../../../../src/app/mock-cars.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CarsService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var CarsService = (function () {
    function CarsService() {
    }
    CarsService.prototype.getCars = function () {
        return Promise.resolve(__WEBPACK_IMPORTED_MODULE_1__mock_cars__["a" /* CARS */]);
    };
    CarsService.prototype.getCar = function (id) {
        return this.getCars()
            .then(function (cars) { return cars.find(function (car) { return car.id === id; }); });
    };
    return CarsService;
}());
CarsService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["l" /* Injectable */])(),
    __metadata("design:paramtypes", [])
], CarsService);

//# sourceMappingURL=cars.service.js.map

/***/ }),

/***/ "../../../../../src/app/confirmation/confirmation.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/confirmation/confirmation.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container col-12 justify-content-center\" style=\"margin-top: 5%;margin-bottom: 35%\">\n\n  <div class=\"row col-12\">\n      <div class=\"col-3\"></div>\n      <div class=\"col-8\">\n        <h2> Thank you for you reservation! </h2>\n        <h3> We look forward to welcoming you to Crete!</h3>\n      </div>\n  </div>\n\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/confirmation/confirmation.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConfirmationComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ConfirmationComponent = (function () {
    function ConfirmationComponent() {
    }
    ConfirmationComponent.prototype.ngOnInit = function () {
    };
    return ConfirmationComponent;
}());
ConfirmationComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_14" /* Component */])({
        selector: 'app-confirmation',
        template: __webpack_require__("../../../../../src/app/confirmation/confirmation.component.html"),
        styles: [__webpack_require__("../../../../../src/app/confirmation/confirmation.component.css")]
    }),
    __metadata("design:paramtypes", [])
], ConfirmationComponent);

//# sourceMappingURL=confirmation.component.js.map

/***/ }),

/***/ "../../../../../src/app/contact/contact.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/contact/contact.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container \" >\n  <div class=\"row justify-content-md-center \" style=\"margin-top: 3%; margin-bottom: 15%\">\n  <div style=\"background-color: #E3E4EE; -moz-border-radius: 13px;-webkit-border-radius: 13px; border-radius: 13px;\">\n    <div style=\"width: 80%;margin-left: 15%; margin-bottom: 30px;\"  itemscope itemtype=\"http://schema.org/Organization\">\n      <br>\n      <h2> Contact details of<span itemprop=\"name\">LibertyCarts.co.uk</span></h2>\n      <br>\n      <p> Email us : <span  itemprop=\"email\">info@libertycars.co.uk</span> </p>\n\n    </div>\n  </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/contact/contact.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ContactComponent = (function () {
    function ContactComponent() {
    }
    ContactComponent.prototype.ngOnInit = function () {
    };
    return ContactComponent;
}());
ContactComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_14" /* Component */])({
        selector: 'app-contact',
        template: __webpack_require__("../../../../../src/app/contact/contact.component.html"),
        styles: [__webpack_require__("../../../../../src/app/contact/contact.component.css")]
    }),
    __metadata("design:paramtypes", [])
], ContactComponent);

//# sourceMappingURL=contact.component.js.map

/***/ }),

/***/ "../../../../../src/app/crete/crete.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/crete/crete.component.html":
/***/ (function(module, exports) {

module.exports = "\n<div class=\"container col-9\">\n  <div class=\"row\">\n    <div itemscope itemtype=\"http://schema.org/TouristAttraction\" style=\"margin-bottom: 15px;\">\n      <div class=\"\"><h1 itemprop=\"name\">Heraklion</h1></div>\n      <div class=\"\" title=\"Source:© www.visitgreece.gr\"><p itemscope itemtype=\"http://schema.org/TouristAttraction\">\n        <span itemprop=\"name\">Heraklion</span>\n        <span itemprop=\"geo\" itemscope itemtype=\"http://schema.org/GeoCoordinates\">\n                <meta itemprop=\"latitude\" content=\"35.3333\" />\n                <meta itemprop=\"longitude\" content=\"25.1333\" />\n              </span>\n        is the largest city of\n        <span itemscope itemtype=\"http://schema.org/TouristAttraction\">\n                 <span itemprop=\"name\"><b>Crete</b> </span>\n                  <meta itemprop=\"sameAs\" content=\"http://en.wikipedia.org/wiki/Crete\" />\n              <span itemprop=\"geo\" itemscope itemtype=\"http://schema.org/GeoCoordinates\">\n                <meta itemprop=\"latitude\" content=\"35.2100\" />\n                <meta itemprop=\"longitude\" content=\"24.9100\" />\n\n              </span>\n            </span>\n        and one of\n        Greece’s major urban centers. Its development begun in the wake of the 9<sup>th</sup> century AD (in antiquity,\n        <span itemscope itemtype=\"http://schema.org/TouristAttraction\">\n                 <span itemprop=\"name\"><b>Knossos</b> </span>\n                 <meta itemprop=\"sameAs\" content=\"http://en.wikipedia.org/wiki/Knossos\" />\n              <span itemprop=\"geo\" itemscope itemtype=\"http://schema.org/GeoCoordinates\">\n                <meta itemprop=\"latitude\" content=\"35.2980\" />\n                <meta itemprop=\"longitude\" content=\"25.1632\" />\n              </span>\n            </span>\n        was the island’s most important centre, followed by Gortyn). In later times,\n        <span itemscope itemtype=\"http://schema.org/TouristAttraction\">\n                 <span itemprop=\"name\"><b>Heraklion</b> </span>\n                   <meta itemprop=\"sameAs\" content=\"http://en.wikipedia.org/wiki/Heraklion\" />\n                    <span itemprop=\"geo\" itemscope itemtype=\"http://schema.org/GeoCoordinates\">\n                        <meta itemprop=\"latitude\" content=\"35.3333\" />\n                        <meta itemprop=\"longitude\" content=\"25.1333\" />\n                      </span>\n               </span>\n        came under\n        Arabic, Venetian and Ottoman rule; its conquerors initially gave it the name <i>Khandaq</i> or <i>Handak</i>&nbsp;which\n        was corrupted to Candia. During the 2004 Olympic Games, the city of Heraklion provided one of the venues for the\n        football tournament.</p>\n\n        <p>Among the most <b>outstanding sights</b> of\n          <span itemscope itemtype=\"http://schema.org/TouristAttraction\">\n                 <span itemprop=\"name\"><b>Heraklion</b> </span>\n                      <meta itemprop=\"sameAs\" content=\"http://en.wikipedia.org/wiki/Heraklion\" />\n                    <span itemprop=\"geo\" itemscope itemtype=\"http://schema.org/GeoCoordinates\">\n                        <meta itemprop=\"latitude\" content=\"35.3333\" />\n                        <meta itemprop=\"longitude\" content=\"25.1333\" />\n                      </span>\n               </span>\n          are the fortification walls that delimit the “old\n          city”. The first fortifications were built by the Arabs and were later reinforced by the Venetians (15<sup>th</sup>\n          century). From the seven bastions, only the <b>Martinengo</b> bastion survives to this day; there visitors\n          will find the tomb of the renowned writer N. Kazantzakis, overlooking the city. From the four gates to the\n          city, only <b>Chanioporta</b> (1570) with the characteristic winged Lion of Saint Marc and the New Gate\n          (1587) at the southern side survive today.</p>\n\n        <p>In the old (Venetian) port, next to the modern facilities, one can see the vaulted tarsanades where ships\n          used to be built, while the western side is dominated by the <b>Koule</b> fortress (16<sup>th</sup>\n          century).</p>\n\n        <p style=\" text-align: center;\"><img alt=\"\" src=\"assets/img/heraclion.jpg\" itemprop=\"image\"\n                                             title=\"Koule fortress. Source:© www.heraklion.gr\"\n                                             id=\"img/heraclion.jpg\"></p>\n\n        <p>In the <b>heart of the city</b> there are many monuments dating to the Middle Ages, a period in which\n          Heraklion witnessed great prosperity. From the port, ascending\n          <span itemscope itemtype=\"http://schema.org/TouristAttraction\">\n                 <span itemprop=\"name\"><b>25 Avgoustou (August) street,</b> </span>\n                    <span itemprop=\"geo\" itemscope itemtype=\"http://schema.org/GeoCoordinates\">\n                        <meta itemprop=\"latitude\" content=\"35.3333\" />\n                        <meta itemprop=\"longitude\" content=\"25.1333\" />\n                      </span>\n               </span>\n          one reaches a\n          square where the church of Agios Titos is found (built in 1872 at the site where a Byzantine church once\n          stood), while next to it lies the Venetian Loggia (16<sup>th</sup> century), a magnificent, ornate arcaded\n          Venetian building decorated with blazons, trophies, etc., which served as a meeting place for the Duke and\n          other noblemen during the Venetian period.</p>\n\n        <p>A typical feature of Heraklion is its Venetian and Turkish fountains, scattered all around the city. The most\n          famous one is the Venetian-style Μorozinifountain<b>,</b> also known as<b> the Lions</b> (1628), a landmark\n          for local inhabitants and visitors alike. Other noteworthy Venetian fountains are the Bembo fountain\n          (1552-1554) on Kornaros Square, the Sagredo fountain (1602-1604) built in the northwestern corner of the\n          Loggia, and Priuli fountain (1666) located near the port.</p>\n\n        <p>The most interesting Turkish fountains are the <b>Charity fountain</b> (1776), next to the Bembo fountain,\n          which today functions as a café, the “Idomeneas fountain” (behind the Historical Museum of Crete), the\n          “Genitsar Aga Fountain” (on Ikarou street), etc.</p>\n\n        <p>Among the <b>churches of Heraklion</b> the one that stands out is the imposing and sizeable Metropolis of\n          Agios Minas (on Agia Ekaterini Square), built between 1862-1895. Adjacent to it, stands the chapel of Mikros\n          Agios Mina<b>s</b> and on the northeast is situated the church of Agia Ekaterini (16<sup>th</sup> century),\n          which functionsas a museum housing exhibits from the Cretan Renaissance. The church of Agios Markos (13<sup>th</sup>\n          century) towers over Venizelou Square, while other important churches in the city are the Monastery of Agios\n          Petros and Pavlos, the Panagia Akrotiriani, the Panagia ton Stavroforon, etc.</p>\n\n        <p>One should not miss out on a visit to the <b>Archaeological Museum</b>, one of the most important museums in\n          Greece; it contains almost all the unique treasures of the Minoan civilization unearthed at Knossos,\n          Phaistos, Malia and other sites. Other museums that are worth visiting are the <b>Historical Museum</b>, the\n          <b>Museum of Natural History</b> and the <b>Acquarium</b><b>Cretaquarium</b> <b>”</b> (in the area of\n          Gournes).</p>\n\n        <p>In the market of Heraklion, one of the richest in the Mediterranean, visitors can find all sorts of modern\n          products, as well as traditional Cretan products such the famous <b>Cretan olive oil</b>, <i>raki</i>, local\n          wine, honey, herbs, etc. Moreover, a modern golf course&nbsp; both for beginners and advanced players\n          operates not far away from the city of Heraklion (in the area of Hersonisos).</p>\n\n        <p>&nbsp;</p>\n\n        <p><i><b>In the environs of Heraklion</b></i></p>\n\n        <p>Five km south of Heraklion lies\n          <span itemscope itemtype=\"http://schema.org/TouristAttraction\">\n                 <span itemprop=\"name\"><b>Knossos,</b> </span>\n                 <meta itemprop=\"sameAs\" content=\"http://en.wikipedia.org/wiki/Knossos\" />\n              <span itemprop=\"geo\" itemscope itemtype=\"http://schema.org/GeoCoordinates\">\n                <meta itemprop=\"latitude\" content=\"35.2980\" />\n                <meta itemprop=\"longitude\" content=\"25.1632\" />\n              </span>\n            </span> one of the most important archeological sites in Europe, the\n          legendary centre of the Minoan civilization from 1900 to 1400 BC. The Palace of Knossos, the largest one in\n          Minoan Crete, witnessed two architectural phases and which was devastated by the earthquake of Santorini\n          (1450 BC). The site contains the remains of the palace of Minos, of dwellings occupied by officials, priests\n          and residents, as well as of cemeteries. The Palace was an intricate building complexbuilt around a central\n          court. &nbsp;It was laid out on a surface of 22,000 m<sup>2</sup> and apart from the royal apartments, it\n          encompassed were also ceremonial quarters, treasure rooms, workshops and storage spaces.</p>\n\n        <p>63 km southwest of Heraklion lie the remains of the\n          <span itemscope itemtype=\"http://schema.org/TouristAttraction\">\n                 <span itemprop=\"name\"> <b>Phaistos</b> </span>\n                    <span itemprop=\"geo\" itemscope itemtype=\"http://schema.org/GeoCoordinates\">\n                        <meta itemprop=\"latitude\" content=\"35.0514\" />\n                        <meta itemprop=\"longitude\" content=\" 24.8136\" />\n                      </span>\n               </span>\n\n          Palace, second in importance\n          in Minoan Crete, inhabited since the Neolithic times. The architectural layout of the palace is identical to\n          the one of Knossos. Here too, rooms are clustered around a paved court. As regards the interior decoration,\n          the Phaistos Palace did not yield many frescoes, however, the floors and walls are covered with stark-white\n          gypsum slabs. The Palace occupied a surface of 9,000 m<sup>2</sup> approximately.</p>\n        <span itemscope itemtype=\"http://schema.org/TouristAttraction\">\n                 <meta itemprop=\"name\" content=\"Knossos\"/>\n\n            <p align=\"center\"><img alt=\"Knossos\"  itemprop=\"image\"\n                                   style=\" padding-left: 5px; padding-bottom: 5px; border: 0; padding-top: 5px; padding-right: 5px;\"\n                                   src=\"assets/img/site_hrakleio_510_2.jpg\"></p>\n            <span itemprop=\"geo\" itemscope itemtype=\"http://schema.org/GeoCoordinates\">\n                        <meta itemprop=\"latitude\" content=\"35.2980\" />\n                        <meta itemprop=\"longitude\" content=\"25.1632\" />\n                      </span>\n            </span>\n        <p>Moreover, in the vicinity of\n          <span itemscope itemtype=\"http://schema.org/TouristAttraction\">\n                 <span itemprop=\"name\"><b>Malia</b> </span>\n                    <span itemprop=\"geo\" itemscope itemtype=\"http://schema.org/GeoCoordinates\">\n                        <meta itemprop=\"latitude\" content=\"35.3000\" />\n                        <meta itemprop=\"longitude\" content=\"25.0000\" />\n                      </span>\n               </span>\n          (34 km east of Heraklion) excavations have brought to light a\n          palace similar to the one of Knossos and Phaistos. At <b>Hrissolakos</b> (Pit of Gold), archaeologists also\n          unearthed the districts surrounding the Minoan palace and cemetery. The palace covered an area of about\n          9,000 m<sup>2</sup>.</p>\n\n        <p>Finally, 46 km south of Heraklion lies the archaeological site of <b>Gortyn,</b> boasting finds from the\n          16<sup>th</sup> century BC up to the 2<sup>nd</sup> century AD.</p></div>\n\n    </div>\n    <div class=\"infoCrete\" title=\"Source:© www.visitgreece.gr\" itemscope itemtype=\"http://schema.org/TouristAttraction\">\n      <div class=\"\" itemprop=\"name\"><h1>Prefecture of Chania</h1></div>\n      <div class=\"\"><p>The Prefecture of\n        <span itemscope itemtype=\"http://schema.org/TouristAttraction\">\n                 <span itemprop=\"name\"><b>Chania</b> </span>\n                    <span itemprop=\"geo\" itemscope itemtype=\"http://schema.org/GeoCoordinates\">\n                        <meta itemprop=\"latitude\" content=\"35.5167\" />\n                        <meta itemprop=\"longitude\" content=\"24.0167\" />\n                      </span>\n               </span>\n        (also spelled Haniá) is the western most division of the island\n        of<b>Crete</b>. The Prefecture of\n        <span itemscope itemtype=\"http://schema.org/TouristAttraction\">\n                 <span itemprop=\"name\"><b>Rethymno</b> </span>\n                    <span itemprop=\"geo\" itemscope itemtype=\"http://schema.org/GeoCoordinates\">\n                        <meta itemprop=\"latitude\" content=\"35.3667\" />\n                        <meta itemprop=\"longitude\" content=\"24.4667\" />\n                      </span>\n               </span>\n        forms its eastern border, whilst sea lap its remaining areas. The\n        inhabited islands of Gavdos and Gavdopoula, which are located at a distance of 20 miles south from Chania, in\n        Liviko Sea, also come under Chania.</p>\n\n        <p>The major cities of the prefecture are Chania, the capital of the prefecture, and Kastelli in Kisamos. Among\n          the most important villages are Paleochora and Kantanos in Selino, the Chora of Sfakion in Sfakia.&nbsp;</p>\n\n        <p itemprop=\"description\">The Prefecture of Chania provides tourist services and activities of all kinds, satisfying all the choices.\n          The city of Chania maintains unaltered all of its characteristics, from the time of the Venetian Rule up\n          until today.&nbsp;</p>\n\n        <p align=\"center\"><img itemprop=\"image\" alt=\"Chania\" src=\"assets/img/Chania_500_2.jpg\"/></p>\n\n        <p>The region of Chania is dominated by the impressive<b> White Mountains</b> (in Greek: Lefká Óri) and its\n          famous National Park, which occupy the largest part of the region.</p>\n\n        <p>The White Mountains’ National Park, expanding around Samaria Gorge, is the biggest and most imposing\n          gorge in Greece. You need about seven hours to cross it but the rich landscape and rare flora and fauna will\n          definitely reward you. There are also many other smaller gorges for you to hike (Aradaina, Agia Irini,\n          Imbros and Polyrhenia) as well as beautiful walking trails (from Hrysoskalitissa to Elafonissi, from\n          Palaiohóra to Souyiá and from Ayia Rouméli to Hóra Sfakion), which make Chania a beloved destination among\n          nature enthusiasts from all over the world. On the edge of a gloriously scenic turquoise lagoon lies the\n          islet of Elafonissi with its ancient-old Cedar Forest.&nbsp;</p>\n\n        <p><b>Need more action?</b> Go mountaineering on White Mountains (there are 4 shelters), climbing on the\n          amazingly vertical slope of Mt. Gigilos, or canoying down the Kládos, Sapounás and Thérissos gorges.</p>\n\n        <p><b>Turquoise waters</b> lap against the white sandy beaches, that lie to the west of the city: Hrissi Akti,\n          Ayia Marina, Áyioi Apóstoloi, Máleme, Kalathás, Stavrós, Plataniás, Kolympári, Falássarna, Ayia Rouméli,\n          Souyiá, Ammoúdi, Fínikas, Vótsala, Loutró, Áyios Pávlos, Pahiá Ámmos, Fragokástello and Gávdos are only some\n          of the beaches where you can bask in the sun. On the islet of Elafonissi, a beach with crystal clear waters\n          and white sand dunes will take your breath away! The whole area forms part of the NATURA network.</p>\n\n        <p align=\"center\"><img alt=\"Chania\" itemprop=\"image\" src=\"assets/img/Chania_500.jpg\"></p>\n\n        <p>A plethora of <b>religious and cultural festivals</b> take place all year long, inviting both locals and\n          visitors to experience the Cretan way of celebrating. Local products have their own prominent position in\n          Chania’s cultural life: participate in the Chestnut Festival in Élos, the Rosewater Festival in Foúrni, or\n          the Wine Festival in Voúves. In May takes place a glorious commemoration of the Battle of Crete in all the\n          municipalities of the region. The Agricultural August is an exhibition of Cretan agricultural products and\n          folklore artefacts. Also, several festivals, conferences or sport events (Venizeleia athletics competition)\n          are organised between May and September, most of which are hosted at a beautiful outdoor theatre located in\n          the east bulwark of the Old Town (“Anatolikí Táfros\").</p>\n\n        <p itemprop=\"description\">No visit to Chania is complete unless you have sampled traditional local specialtie eggs with stáka, Cretan\n          kalitsoúnia (sweet mini cheese pies), lamb served with spiny chicory, dácos (the traditional hard Cretan\n          bread accompanied with tomato, mizithra cheese and plenty of virgin Cretan oil), snails boubouristí(popping\n          fried snails), haniótiko bouréki (patty from Chania, a vegetable specialty), kserotígana (honey dipped\n          spiral pastries) wedding cookies, dry bread wreaths, yraviéra cheese (full fat sheep’s cheese with\n          appellation of controlled origin), sweet smelling anthótyros from Sfakiá (fresh, soft, white cheese made of\n          either sheep’s or goat’s milk), fresh stáka butter (the cream of the butter) for the Cretan wedding rice\n          (rice cooked in meat broth), roasted goat or sea food delights – special ingredients blended in delicious\n          sea-urchin salads, or divine fish soups! Accompany your dinner with a glass of deep-red Cretan wine, the\n          divine marouvás, or drink after your meal an ice-cold rakí, a traditional Cretan spirit distilled from\n          pomace, with a delicate aroma of ripe grapes.</p></div>\n\n    </div>\n\n    <div class=\"infoCrete\" title=\"Source:© www.visitgreece.gr\">\n      <div class=\"\"><h1>Discover magical Rethymno</h1></div>\n      <div class=\"\" itemscope itemtype=\"http://schema.org/TouristAttraction\" >\n        <p itemprop=\"description\">The city of\n          <span itemscope itemtype=\"http://schema.org/TouristAttraction\">\n                 <span itemprop=\"name\"><b>Rethymno</b> </span>\n                    <span itemprop=\"geo\" itemscope itemtype=\"http://schema.org/GeoCoordinates\">\n                        <meta itemprop=\"latitude\" content=\"35.3667\" />\n                        <meta itemprop=\"longitude\" content=\"24.4667\" />\n                      </span>\n               </span>\n          is one of the best preserved medieval towns in Greece: Venetian fortification works\n          mingle harmonically with orthodox and catholic churches, mosques, grand mansions of Venetian architecture,\n          arches and cobblestone streets they all create a wondrous atmosphere.</p>\n\n        <p>Crete's smallest prefecture located between White Mountains and Mt Psilorítis (also called “Ídi”),\n          is synonymous with gorgeous mountainscapes, marvellous beaches, Cretan lyre melodies, tsikoudiá spirit served\n          with “oftó”, legendary caves, historic monasteries and monuments, traditional mountain villages and luxurious\n          holiday resorts. Feel the essence of <b>Mythical Crete </b>in this mountainous, remote and self-sufficient\n          region of the island of Crete.</p>\n\n\n\n        <p align=\"center\"><a href=\"http://www.flickr.com/photos/visitgreecegr/4456108175/\"\n                             title=\"Rethimno by Visit Greece, on Flickr\"><img height=\"282\" alt=\"Rethimno\" itemprop=\"image\"\n                                                                              width=\"500\"\n                                                                              src=\"http://farm5.static.flickr.com/4013/4456108175_c3da0b398c.jpg\"></a>\n        </p>\n\n        <p>Réthymno’s outstanding <b>natural wealth</b> is reflected on Mt. Psiloritis, which dominates the eastern part\n          of the region, the most mountainous part on the island. The variation of the landscape will impress the\n          nature enthusiasts: flourishing valleys succeed harsh mountainscapes and rocky shores follow long sandy\n          beaches. Steep gorges, leafy valleys, small rivers cutting through the mountains, wild life refuges and\n          forty canyons complete the picture. Unique wonders of nature will take your breath away:</p>\n\n        <p><b>• Lagoon of Préveli:</b> At the point where river Meyálos Potamós (“Big River”) flows into the sea and\n          “Kourtaliotis” gorge ends lie the famous Préveli Lagoon and “Palm beach” (“Fínikas”), a sandy cove with a\n          small date-palm grove. To get there you have to follow the road to the Monastery of Préveli. Shortly before\n          the monastery a track on your left leads down to a parking place. From this point onwards walk down to the\n          sandy beach, where a remarkable, almost tropical landscape awaits you. The river flowing into the sea\n          combined with the rich vegetation creates a magnificent sight. Don’t miss it!</p>\n\n        <p><b>•</b> <b>The Nída Plateau</b> is located 79km far from Réthymno, on Mount Psilorítis. Here, major\n          attractions are “mitáta\", vaulted stone huts where the shepherds live. The Plateau provides also skiing\n          facilities during wintertime.<br> <b>•</b> <b>Argyroúpoli</b>: 27km far from Réthymno you will find\n          Argyroúpoli, a village built on the remnants of the ancient city of Láppas. Numerous springs, the cave and\n          the chapel bearing the same name are all well worth a visit.<br> <b>•</b> <b>Gorges</b> of extraordinary\n          beauty traverse the mountains of the region: the ravine of Kourtaliótis, 3km long, ends at the famous Lagoon\n          of Préveli; the ravine of Kotsifoú starts from the village of Kánevos and ends near the village of Sellía;\n          the gorge of Patsós, in the Amári district; the gorge of Prassés, which ends at the village of Plataniás at\n          the north coast east of the town of Réthymno; finally, the gorge of Arkádi and a number of smaller ones.<br>\n          <b>•</b> The <b>mountains of the region</b> are exceptionally rich in caves. The most famous caves are those\n          of Geráni, Simonélli west of the town of Réthymno, Áyios Antónios in the district of Amári, Melidóni, Moúgri\n          Sissón and Sfendóni near the village of Zonianá. The cave of Idéon Ándron, in which Zeus was raised\n          according to mythology, represented an important place of worship in both the Minoan and the Roman periods.\n        </p>\n\n        <p>Important archaeological finds indicate that the area have flourished from the Stone Age up until the Roman\n          and Early Christian periods. Minoan and Geometric sites, cemeteries, Roman cities and Hellenistic relics\n          have been discovered, most important of which are considered to be Eléftherna, an ancient settlement\n          inhabited until the 8th century, as well as Arménon cemetery with more than 350 underground tombs.\n          Ecclesiastic monuments like stone chapels on Mt Psiloritis, historical monasteries and early Christian\n          Basilicas enrich your visit on the island. A monastery of great historic importance is the 15th century\n          Arkádi Monastery overlooking the imposing gorge and Préveli. Set off on a journey back to time through your\n          visit to traditional settlements like Ádele, Anóyia, Rústika, Garázo and Chromonastíri and feel their\n          original Cretan atmosphere.</p>\n\n        <p>Here you can taste delicious <b>local culinary delights</b>, like goat cooked with tomatoes, hilopítes (home\n          made pasta) and snails with vegetables –always accompanied with a shot of local tsikoudiá!</p>\n\n        <p>Visit <b>Réthymno</b> in the summer and bask on its sun soaked <b>beaches</b>. Magnificent sandy beaches\n          await the beachgoers on the north coast (at the Cretan Sea) as well as on the south coast of the island (at\n          the Libyan Sea). Some of them are:<br> <b>• Réthymno Beach</b>: Organised sandy beach more than 20 km long,\n          stretching from Réthymno town across the east side of the area.<br> <b>•</b> <b>Pánormos</b>: This coastal\n          village is situated 20 km east of Réthymno. It boasts an organised beach with many tavérnas and\n          hotels.<b><br> •</b>\n          <b>Bali</b>: Nestling in the surrounding mountains of the Bali bay you will find the\n          coastal village of Bali, 34 km east of Réthymno. Choose one of the small islets and enjoy sun bathing at\n          organised beaches.<br> <b>•</b> <b>Plakiás</b>: The village of Plakiás is situated 40 km south of Réthymno.\n          This resort boasts a superb sandy beach and a well-organised tourist infrastructure including water sport\n          facilities.<br> <b>•</b> <b>Ayía Galíni</b>: situated 58km south of Réthymno, the resort offers a\n          well-organised tourist infrastructure and crystal clear beaches hidden on the surrounding islets.<br>\n          <b>•</b> <b>Préveli beach</b>: The beach of Préveli or “Palm Beach” (Fínikas) is situated at the southern\n          coast of Crete, 40 km far from Réthymno. The palm-tree background and the small lagoon turn this smooth\n          sandy beach into a truly unique exotic place!</p>\n\n        <p>Don’t forget to visit Réthymno <b>during the summer</b> months to attend two fascinating events only to be\n          found here: the famous “Renaissance Festival” invites you to become protagonists, co-creators and\n          co-travellers in an enchanting trip through time where music and theatrical expression prevail. Almost all\n          performances take place at the “Erofili” theatre in Fortezza. The <b>“Wine Festival”</b>, on the other hand,\n          welcomes wine producers, wine aficionados and culinary enthusiasts to a pan Cretan event of culture,\n          gastronomy, wine tasting, entertainment and fun!</p></div>\n\n\n    </div>\n    <div class=\"infoCrete\" title=\"Source:© www.visitgreece.gr\">\n      <div class=\"\"><h1>Ayios Nikolaos</h1></div>\n      <div class=\"\"><p>   <span itemscope itemtype=\"http://schema.org/TouristAttraction\">\n                 <span itemprop=\"name\"><b>Ayios Nikolaos</b> </span>\n                    <span itemprop=\"geo\" itemscope itemtype=\"http://schema.org/GeoCoordinates\">\n                        <meta itemprop=\"latitude\" content=\"35.1833\" />\n                        <meta itemprop=\"longitude\" content=\"25.7167\" />\n                      </span>\n               </span> (“Ag Nik” as the British visitors love to call it) is the capital\n        town of <b>Lassithi</b>. Here, the bottomless salt lake Voulisméni dominates\n        the area. A narrow channel of water connects the lake with the sea, while an imposing backdrop of red rock and\n        trees adds to the natural beauty of the scenery. A small pine tree park lies above the lake, and a stone path\n        leads to its southern section to a cute small harbour for fishing boats. The city boasts interesting\n        Archaeological, Folklore, and Natural History <b>museums</b>, Byzantine churches, a <b>well-organised marina</b>,\n        bustling pedestrian streets (ideal for leisurely walks), and traditional squares with buzzing cafés and\n        restaurants.</p>\n\n        <p align=\"center\"><img  alt=\"Lassithi\"    title=\"Agio Nick. Source:© http://www.visitgreece.gr/en/main_cities/ayios_nikolaos\"\n                                src=\"assets/img/site_lasithi_510_1.jpg\"></p>\n\n        <p><b>During your stay here, pay a visit to:<br> •</b> The island of Ayioi Pantes that houses a breeding\n          facility for the Cretan wild goat (kri kri).<br> <b>•</b> Almyrós wetland, a passage for migratory\n          birds.<br> <b>•</b> Istro, 11 km S, is famous for its consecutive beaches at Karavostássi, Áyios\n          Panteleimonas and Voúlisma (Hrissí Aktí).<br> <b>•</b> Kritsa, 11 km SW, is a picturesque village built on\n          the feet of Mt. Kastellos with a marvellous view to Mirabello bay. Its traditional architectural features\n          are impressively preserved through time. Here was actually shot in 1956 the famous Jules Dassin movie\n          “Christ Recrucified”.<br> <b>•</b>&nbsp;Pachia Ammos, 20 km SE, is an old commercial port at the junction of\n          Áyios Nikolaos-Sitía-Ierápetra.</p></div>\n\n\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/crete/crete.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CreteComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var CreteComponent = (function () {
    function CreteComponent() {
    }
    CreteComponent.prototype.ngOnInit = function () {
    };
    return CreteComponent;
}());
CreteComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_14" /* Component */])({
        selector: 'app-crete',
        template: __webpack_require__("../../../../../src/app/crete/crete.component.html"),
        styles: [__webpack_require__("../../../../../src/app/crete/crete.component.css")]
    }),
    __metadata("design:paramtypes", [])
], CreteComponent);

//# sourceMappingURL=crete.component.js.map

/***/ }),

/***/ "../../../../../src/app/footer/footer.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".footer {\n  font-size: small;\n  text-align: center;\n  background-color: #001f3f;\n  color: #F7F3F3;\n  width: 100%;\n}\n\n.separator {\n  background-color: #0074D9;\n  height: 30px;\n}\n\n.info {\n  width: 32.9%;\n  height: 120px;\n  float: left;\n  clear: right;\n}\n\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/footer/footer.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"footer\">\n  <nav class=\"navbar navbar-light bg-faded navbar-fixed-bottom navbar-toggleable-md\">\n      <ul class=\"navbar-nav navbar-collapse\">\n        <li class=\"nav-item\"><a routerLink=\"/terms\" routerLinkActive=\"active\" class=\"nav-link\">Terms and conditions</a></li>\n        <li class=\"nav-item\"><a routerLink=\"/privacy\" routerLinkActive=\"active\" class=\"nav-link\">Privacy policy</a></li>\n        <li class=\"nav-item\"><a routerLink=\"/main\" routerLinkActive=\"active\" class=\"nav-link\">Sitemap</a></li>\n        <li class=\"nav-item\"><a routerLink=\"/contact\" routerLinkActive=\"active\" class=\"nav-link\">Contact us</a></li>\n      </ul>\n    <span class=\"navbar-text navbar-brand\" id=\"createdBy\" style=\"font-size: 0.9rem;\">\n      Created by <a href=\"http://ltatakis.com\">Lefteris Tatakis</a>\n    </span>\n  </nav>\n  <footer>\n    <p itemscope itemtype=\"http://schema.org/CreativeWork\">\n    Copyright &copy; <span itemprop=\"copyrightYear\">2017</span><span itemprop=\"copyrightHolder\"> LibertyCars.co.uk </span>\n    - All Rights\n    Reserved.</p>\n  </footer>\n</div>\n\n\n"

/***/ }),

/***/ "../../../../../src/app/footer/footer.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__ = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FooterComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var FooterComponent = FooterComponent_1 = (function () {
    function FooterComponent() {
    }
    FooterComponent.prototype.ngOnInit = function () {
    };
    return FooterComponent;
}());
FooterComponent = FooterComponent_1 = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["b" /* NgModule */])({
        declarations: [FooterComponent_1],
        imports: [__WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__["a" /* NgbModule */]]
    }),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_14" /* Component */])({
        selector: 'app-footer',
        template: __webpack_require__("../../../../../src/app/footer/footer.component.html"),
        styles: [__webpack_require__("../../../../../src/app/footer/footer.component.css")]
    }),
    __metadata("design:paramtypes", [])
], FooterComponent);

var FooterComponent_1;
//# sourceMappingURL=footer.component.js.map

/***/ }),

/***/ "../../../../../src/app/header/header.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".menu {\n  background-color: #001f3f;\n  color: #FBFBFB;\n  height: 4%;\n  vertical-align: middle;\n  text-align: center; /* FF1+ */ /* Saf3-4 */\n  border-radius: 12px; /* Opera 10.5, IE 9, Saf5, Chrome */\n}\n\n/*.pageMenu {*/\n  /*width: 100%;*/\n/*}*/\n\n/*.fluid {*/\n  /*clear: both;*/\n  /*margin-left: 0;*/\n  /*width: 100%;*/\n  /*float: left;*/\n  /*display: block;*/\n/*}*/\n\n/*.fluidList {*/\n  /*list-style:none;*/\n  /*list-style-image:none;*/\n  /*margin:0;*/\n  /*padding:0;*/\n/*}*/\n\n/*.zeroMargin {*/\n  /*margin-left: 0;*/\n/*}*/\n/*.item {*/\n  /*width: 7%;*/\n  /*!*margin-left: 3.4482%;*!*/\n  /*clear: none;*/\n  /*height: 4%;*/\n  /*font-size: large;*/\n  /*margin-top: .2%;*/\n  /*margin-bottom: .2%;*/\n  /*margin-left: 20%;*/\n/*}*/\n\n/*.header {*/\n  /*width: 100%;*/\n  /*font-family: Baskerville, \"Palatino Linotype\", Palatino, \"Century Schoolbook L\", \"Times New Roman\", serif;*/\n  /*font-style: normal;*/\n  /*font-size: xx-large;*/\n  /*!*text-indent: 30px;*!*/\n  /*!*height: 50px;*!*/\n/*}*/\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/header/header.component.html":
/***/ (function(module, exports) {

module.exports = "\n  <nav class=\"navbar navbar-toggleable-sm navbar-inverse bg-faded\" style=\"background-color: #001f3f;\">\n    <a class=\"navbar-brand\" [routerLink]=\"['/home']\">\n      <img src=\"/assets/eagle.png\" width=\"30\" height=\"30\" class=\"d-inline-block align-top\" alt=\"\">\n      Liberty Cars\n    </a>\n    <button type=\"button\" class=\"navbar-toggler navbar-toggler-right\" (click)=\"isExpanded = !isExpanded\" [attr.aria-expanded]=\"!isExpanded\" aria-controls=\"navbarContent\">\n      <span class=\"navbar-toggler-icon\"></span>\n    </button>\n\n    <div class=\"collapse navbar-collapse\" id=\"navbarContent\" [ngbCollapse]=\"!isExpanded\">\n      <ul class=\"navbar-nav mr-auto \" style=\"text-align:center\">\n        <li class=\"nav-item active\"><a routerLink=\"/main\" routerLinkActive=\"active\" class=\"nav-link\">Home</a></li>\n        <li class=\"nav-item\"><a routerLink=\"/crete\" routerLinkActive=\"active\" class=\"nav-link\">Crete</a></li>\n        <li class=\"nav-item\"><a routerLink=\"/contact\" routerLinkActive=\"active\" class=\"nav-link\">Contact us</a></li>\n      </ul>\n    </div>\n  </nav>\n"

/***/ }),

/***/ "../../../../../src/app/header/header.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HeaderComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HeaderComponent = (function () {
    function HeaderComponent() {
    }
    HeaderComponent.prototype.ngOnInit = function () {
    };
    return HeaderComponent;
}());
HeaderComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_14" /* Component */])({
        selector: 'app-header',
        template: __webpack_require__("../../../../../src/app/header/header.component.html"),
        styles: [__webpack_require__("../../../../../src/app/header/header.component.css")]
    }),
    __metadata("design:paramtypes", [])
], HeaderComponent);

//# sourceMappingURL=header.component.js.map

/***/ }),

/***/ "../../../../../src/app/locations.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__mock_locations__ = __webpack_require__("../../../../../src/app/mock-locations.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LocationsService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var LocationsService = (function () {
    function LocationsService() {
    }
    LocationsService.prototype.getLocations = function () {
        return __WEBPACK_IMPORTED_MODULE_1__mock_locations__["a" /* LOCATIONS */];
    };
    LocationsService.prototype.getLocation = function (id) {
        return this.getLocations().find(function (location) { return location.id === id; });
    };
    return LocationsService;
}());
LocationsService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["l" /* Injectable */])(),
    __metadata("design:paramtypes", [])
], LocationsService);

//# sourceMappingURL=locations.service.js.map

/***/ }),

/***/ "../../../../../src/app/main/main.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".search {\n  background-color: #001f3f;\n  color: #FBFBFB;\n  height: 4%;\n  vertical-align: middle;\n  text-align: center; /* FF1+ */ /* Saf3-4 */\n  border-radius: 12px; /* Opera 10.5, IE 9, Saf5, Chrome */\n}\n\n.background {\n  z-index: -99;\n  position: absolute;\n  top: 2%;\n  right: 0;\n  width: 100%; height: 100%;\n  background: url(" + __webpack_require__("../../../../../src/assets/hat.jpg") + ");\n  background-size: cover;\n  -moz-background-size: cover;\n}\n\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/main.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"background\"></div>\n\n<div  class=\"container\" style=\"margin-top: 2%; margin-bottom: 20%;\" >\n  <div style=\"text-align:center\" >\n  <h1> Welcome to {{title}}</h1>\n    <br/>\n  </div>\n\n\n  <div>\n    <div class=\"justify-content-md-centre justify-content-lg-left col-xs-12 col-md-8 col-lg-5\">\n    <form class=\"search\" style=\"margin-bottom: 2%\">\n\n      <div class=\"form-group form-inline row\" style=\"text-align:center\">\n        <h3 class=\"col-12 mt-sm-4\"> Search for best car hire offers:</h3>\n      </div>\n\n      <div class=\"row mb-2\">\n        <label for=\"pickupAddress\" class=\"col-3 col-form-label offset-1\">Pick up:</label>\n        <select class=\"mx-sm-4 col-5\" id=\"pickupAddress\" [(ngModel)]=\"selectedPickUp\" [ngModelOptions]=\"{standalone: true}\">\n          <option *ngFor='let location of locations' [ngValue]='location'>{{location.name}}</option>\n        </select>\n      </div>\n\n      <div class=\"row\">\n        <label for=\"returnAddress\" class=\"col-3 col-form-label offset-1\">Return:</label>\n        <select class=\" mx-sm-4 col-5\" id=\"returnAddress\" [(ngModel)]=\"selectedReturn\" [ngModelOptions]=\"{standalone: true}\">\n          <option *ngFor='let location of locations' [ngValue]='location'>{{location.name}}</option>\n        </select>\n\n      </div>\n\n      <div class=\"form-group form-inline row\">\n        <div class=\"col-5 offset-1\" >\n\n          <div class=\"row justify-content-center\">\n            <label class=\"col-form-label\">Pick up</label>\n          </div>\n\n          <div class=\"row justify-content-center\">\n            <input class=\"form-control col-11\" [ngModel]=\"reservation.datePickUp | date:'yyyy-MM-dd'\" (ngModelChange)=\"reservation.datePickUp = $event\" type=\"date\" name=\"date-pickup\"/>\n          </div>\n\n          <div class=\"row justify-content-center col-12 ml-sm-1 ml-xs-4\" style=\"margin-top: 3%\">\n            <select class=\"form-control custom-select\" id=\"pickupTime\" [(ngModel)]=\"pickUpTime\" [ngModelOptions]=\"{standalone: true}\">\n              <option [ngValue]=\"1000\">10:00</option>\n              <option [ngValue]=\"1030\">10:30</option>\n              <option [ngValue]=\"1100\">11:00</option>\n              <option [ngValue]=\"1130\">11:30</option>\n              <option [ngValue]=\"1200\">12:00</option>\n              <option [ngValue]=\"1230\">12:30</option>\n              <option [ngValue]=\"1300\">13:00</option>\n            </select>\n          </div>\n        </div>\n\n        <div class=\"col-5 ml-xs-2\">\n          <div class=\"row justify-content-center\">\n            <label class=\"col-form-label\">Return</label>\n          </div>\n\n          <div class=\"row justify-content-center\">\n            <input class=\"form-control col-11\" [ngModel]=\"reservation.dateReturn | date:'yyyy-MM-dd'\" (ngModelChange)=\"reservation.dateReturn = $event\" type=\"date\" name=\"date-return\"/>\n\n          </div>\n\n          <div class=\"row justify-content-center col-12 ml-sm-1 ml-xs-4\" style=\"margin-top: 3%\">\n            <select class=\"form-control custom-select\" id=\"returnTime\" [(ngModel)]=\"returnTime\" [ngModelOptions]=\"{standalone: true}\">\n              <option [ngValue]=\"1000\">10:00</option>\n              <option [ngValue]=\"1030\">10:30</option>\n              <option [ngValue]=\"1100\">11:00</option>\n              <option [ngValue]=\"1130\">11:30</option>\n              <option [ngValue]=\"1200\">12:00</option>\n              <option [ngValue]=\"1230\">12:30</option>\n              <option [ngValue]=\"1300\">13:00</option>\n            </select>\n          </div>\n        </div>\n      </div>\n\n      <div class=\"form-group form-inline row\">\n        <div class=\"col-6 offset-5\" >\n          <button type=\"submit\" class=\"btn btn-primary\"\n                  [routerLink]=\"['/search', {\n                  dtr: reservation.dateReturn ,\n                  dpu: reservation.datePickUp,\n                  pl: selectedPickUp.id,\n                  rl: selectedReturn.id,\n                  pT: pickUpTime,\n                  rT: returnTime\n                 }]\" routerLinkActive=\"active\"> Search for prices </button>\n        </div>\n      </div>\n      <div class=\"row\"></div>\n\n    </form>\n    </div>\n  </div>\n</div>\n\n\n<div class=\"container\" style=\"background-color: gainsboro; margin-bottom: 2%;\">\n  <div class=\" col-md-12\" style=\"text-align:center\">\n    <div class=\"row justify-content-md-centre \" >\n        <h2 class=\"col-12\" style=\"margin-top: 2%; margin-bottom: 2%\">OUR OFFERS INCLUDE EVERYTHING NO HIDDEN COSTS</h2>\n    </div>\n\n    <div class=\"row col-12\">\n      <div class=\"col-sx-12 col-sm-4  \">\n        <div class=\"row\" >\n          <h2 class=\"col-12\" style=\"text-align:center\"> What we offer</h2>\n          <img src=\"assets/thumbsup.jpeg\" style=\"width:90%; height: 230px; margin-left: 5%\" alt=\"Thumbs up\">\n        </div>\n        <div class=\"row\" >\n          <ul style=\"text-align:left\">\n            <li>New cars - Models 2015-2016</li>\n            <li>Free Pick-Up / Return to/from Airports - Hotels</li>\n            <li>24 Hours road assistance all over the island of Crete</li>\n            <li>Roof racks, road maps, baby seats.</li>\n            <li>Free Klms</li>\n            <li>All Local Taxes</li>\n            <li>VAT 23%</li>\n            <li>Insurance for wheels,underside of car, glasses & mirrors No excess.</li>\n          </ul>\n        </div>\n      </div>\n      <div class=\"col-sx-12 col-sm-4\">\n        <div class=\"row\" >\n          <h2 class=\"col-12\"> Our Insurance</h2>\n          <img src=\"assets/thumbsup.jpeg\" style=\"width:90%; height: 230px;  margin-left: 5%\" alt=\"Thumbs up\">\n\n        </div>\n        <div class=\"row\" >\n          <ul style=\"text-align:left\">\n            <li>Public liability - Third Part.</li>\n            <li>Personal Accident Insurance for passengers & drivers.</li>\n            <li>Theft insurance with <strong>No excess</strong>.</li>\n            <li>Fire Insurance with <strong>No excess</strong>.</li>\n            <li>Full Insurance (Collision Damage Waiver) with No excess.</li>\n            <li>Special Full Insurance 100%, with No excess.</li>\n          </ul>\n        </div>\n      </div>\n      <div class=\"col-sx-12 col-sm-4\">\n        <div class=\"row\" >\n          <h2 class=\"col-12\"> Reservation Info</h2>\n          <img src=\"assets/thumbsup.jpeg\" style=\"width:90%; height: 230px;  margin-left: 5%\" alt=\"Thumbs up\">\n        </div>\n        <div class=\"row\" >\n          <ul style=\"text-align:left\">\n            <li>No hidden extras</li>\n            <li>No credit card required!</li>\n            <li>Cancellation fee 0%</li>\n            <li>Book Now Pay On Arrival !</li>\n            <li>All major Credit Cards accepted</li>\n            <li>Stations all Over Crete Island</li>\n            <li>Leave the rest to us!</li>\n          </ul>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n\n\n      <!--<select name=\"pickUpTime\" ng-model=\"data.pickUpTime\" class=\"form-control pickUpTime\">-->\n        <!--<option value=\"000\">00:00</option>-->\n        <!--<option value=\"010\">01:00</option>-->\n        <!--<option value=\"020\">02:00</option>-->\n        <!--<option value=\"030\">03:00</option>-->\n        <!--<option value=\"040\">04:00</option>-->\n        <!--<option value=\"050\">05:00</option>-->\n        <!--<option value=\"060\">06:00</option>-->\n        <!--<option value=\"070\">07:00</option>-->\n        <!--<option value=\"080\">08:00</option>-->\n        <!--<option value=\"090\">09:00</option>-->\n        <!--<option value=\"095\">09:30</option>-->\n        <!--<option value=\"100\">10:00</option>-->\n        <!--<option value=\"105\">10:30</option>-->\n        <!--<option value=\"110\">11:00</option>-->\n        <!--<option value=\"115\">11:30</option>-->\n        <!--<option value=\"120\" selected=\"selected\">12:00</option>-->\n        <!--<option value=\"125\">12:30</option>-->\n        <!--<option value=\"130\">13:00</option>-->\n        <!--<option value=\"135\">13:30</option>-->\n        <!--<option value=\"140\">14:00</option>-->\n        <!--<option value=\"145\">14:30</option>-->\n        <!--<option value=\"150\">15:00</option>-->\n        <!--<option value=\"155\">15:30</option>-->\n        <!--<option value=\"160\">16:00</option>-->\n        <!--<option value=\"163\">16:30</option>-->\n        <!--<option value=\"170\">17:00</option>-->\n        <!--<option value=\"175\">17:30</option>-->\n        <!--<option value=\"180\">18:00</option>-->\n        <!--<option value=\"185\">18:30</option>-->\n        <!--<option value=\"190\">19:00</option>-->\n        <!--<option value=\"200\">20:00</option>-->\n        <!--<option value=\"210\">21:00</option>-->\n        <!--<option value=\"220\">22:00</option>-->\n        <!--<option value=\"230\">23:00</option>-->\n      <!--</select>-->\n    <!--</div>-->\n  <!--</div>-->\n  <!--<div class=\"fluid pickup\" style=\"margin-left:5%;\">-->\n    <!--<div class=\"fluid\">-->\n      <!--<label for=\"select3\">Drop off:</label>-->\n    <!--</div>-->\n    <!--<div class=\"fluid\"  itemscope itemtype=\"http://schema.org/TouristAttraction\">-->\n      <!--<select name=\"selectArea\" class=\"form-control selectArea\" ng-model=\"data.returnTo\" required id=\"select3\">-->\n        <!--<optgroup label=\"Heraklion Area\">-->\n          <!--<option itemprop=\"name\" value=\"Heraklion_Airport\" selected=\"selected\">Heraklion Airport</option>-->\n          <!--<option itemprop=\"name\" value=\"Heraklion_Port\">Heraklion Port</option>-->\n          <!--<option itemprop=\"name\" value=\"Heraklion_Downtown\">Heraklion</option>-->\n          <!--<option itemprop=\"name\" value=\"Ammoudara\">Ammoudara</option>-->\n          <!--<option itemprop=\"name\" value=\"Lygaria\">Lygaria</option>-->\n          <!--<option itemprop=\"name\" value=\"Agia_Pelagia\">Agia Pelagia</option>-->\n          <!--<option itemprop=\"name\" value=\"Fodele\">Fodele</option>-->\n          <!--<option itemprop=\"name\" value=\"Kokkini_Hani\">Kokkini Hani</option>-->\n          <!--<option itemprop=\"name\" value=\"Gournes\">Gournes</option>-->\n          <!--<option itemprop=\"name\" value=\"Gouves\">Gouves</option>-->\n          <!--<option itemprop=\"name\" value=\"Analipsi\">Analipsi</option>-->\n          <!--<option itemprop=\"name\" value=\"Anissaras\">Anissaras</option>-->\n          <!--<option itemprop=\"name\" value=\"Chersonissos\">Chersonissos</option>-->\n          <!--<option itemprop=\"name\" value=\"Koutouloufari\">Koutouloufari</option>-->\n          <!--<option itemprop=\"name\" value=\"Piskopiano\">Piskopiano</option>-->\n          <!--<option itemprop=\"name\" value=\"Stalis\">Stalis</option>-->\n          <!--<option itemprop=\"name\" value=\"Malia\">Malia</option>-->\n          <!--<option itemprop=\"name\" value=\"Matala\">Matala</option>-->\n        <!--</optgroup>-->\n        <!--<optgroup label=\"Rethymno Area\">-->\n          <!--<option itemprop=\"name\" value=\"Rethymno_Port\">Rethymno Port</option>-->\n          <!--<option itemprop=\"name\"  value=\"Rethymno_Downtown\">Rethymno</option>-->\n          <!--<option itemprop=\"name\" value=\"Scaleta\">Scaleta</option>-->\n          <!--<option itemprop=\"name\" value=\"Panormo\">Panormo</option>-->\n          <!--<option itemprop=\"name\" value=\"Georgioupolis\">Georgioupolis</option>-->\n          <!--<option itemprop=\"name\" value=\"Kavros\">Kavros</option>-->\n          <!--<option itemprop=\"name\" value=\"Bali\">Bali</option>-->\n        <!--</optgroup>-->\n        <!--<optgroup label=\"Chania Area\">-->\n          <!--<option itemprop=\"name\" value=\"Chania_Airport\">Chania Airport</option>-->\n          <!--<option itemprop=\"name\" value=\"Chania_Port\">Chania Port</option>-->\n          <!--<option itemprop=\"name\" value=\"Chania_Downtown\">Chania</option>-->\n          <!--<option itemprop=\"name\" value=\"Agia_Marina\">Agia Marina</option>-->\n          <!--<option itemprop=\"name\" value=\"Platanias\">Platanias</option>-->\n        <!--</optgroup>-->\n        <!--<optgroup label=\"Agios Nikolaos Area\">-->\n          <!--<option itemprop=\"name\" value=\"Agios_Nikolaos_Port\">Agios Nikolaos Port</option>-->\n          <!--<option itemprop=\"name\"  value=\"Agios_Nikolaos\">Agios Nikolaos</option>-->\n          <!--<option itemprop=\"name\" value=\"Sisi\">Sisi</option>-->\n          <!--<option itemprop=\"name\" value=\"Ierapetra\">Ierapetra</option>-->\n          <!--<option itemprop=\"name\" value=\"Sitia\">Sitia</option>-->\n        <!--</optgroup>-->\n      <!--</select>-->\n    <!--</div>-->\n    <!--<br/>-->\n\n    <!--<div class=\"fluid\" style=\"margin-top: 5px;\" ng-controller=\"DateReturnCtrl\">-->\n      <!--<input type=\"text\" class=\"form-control\" is-open=\"opened\"/>-->\n      <!--<button class=\"btn btn-default glyph\" ng-click=\"open($event)\"><i class=\"glyphicon glyphicon-calendar\"></i>-->\n      <!--</button>-->\n      <!--<select name=\"pickUpTime\" ng-model=\"data.returnTime\" class=\" form-control pickUpTime \" id=\"putime2\">-->\n        <!--<option value=\"000\">00:00</option>-->\n        <!--<option value=\"010\">01:00</option>-->\n        <!--<option value=\"020\">02:00</option>-->\n        <!--<option value=\"030\">03:00</option>-->\n        <!--<option value=\"040\">04:00</option>-->\n        <!--<option value=\"050\">05:00</option>-->\n        <!--<option value=\"060\">06:00</option>-->\n        <!--<option value=\"070\">07:00</option>-->\n        <!--<option value=\"080\">08:00</option>-->\n        <!--<option value=\"090\">09:00</option>-->\n        <!--<option value=\"095\">09:30</option>-->\n        <!--<option value=\"100\">10:00</option>-->\n        <!--<option value=\"105\">10:30</option>-->\n        <!--<option value=\"110\">11:00</option>-->\n        <!--<option value=\"115\">11:30</option>-->\n        <!--<option value=\"120\" selected=\"selected\">12:00</option>-->\n        <!--<option value=\"125\">12:30</option>-->\n        <!--<option value=\"130\">13:00</option>-->\n        <!--<option value=\"135\">13:30</option>-->\n        <!--<option value=\"140\">14:00</option>-->\n        <!--<option value=\"145\">14:30</option>-->\n        <!--<option value=\"150\">15:00</option>-->\n        <!--<option value=\"155\">15:30</option>-->\n        <!--<option value=\"160\">16:00</option>-->\n        <!--<option value=\"163\">16:30</option>-->\n        <!--<option value=\"170\">17:00</option>-->\n        <!--<option value=\"175\">17:30</option>-->\n        <!--<option value=\"180\">18:00</option>-->\n        <!--<option value=\"185\">18:30</option>-->\n        <!--<option value=\"190\">19:00</option>-->\n        <!--<option value=\"200\">20:00</option>-->\n        <!--<option value=\"210\">21:00</option>-->\n        <!--<option value=\"220\">22:00</option>-->\n        <!--<option value=\"230\">23:00</option>-->\n      <!--</select>-->\n    <!--</div>-->\n  <!--</div>-->\n  <!--<div class=\"fluid\" style=\"height: 15px;\">-->\n  <!--</div>-->\n  <!--<div class=\"fluid choice\" style=\"margin-left: 5%;\">-->\n    <!--<label>Category: </label> <select>-->\n    <!--<option itemscope itemtype=\"http://schema.org/category\" itemprop=\"name\" value=\"\">Economy</option>-->\n    <!--<option itemscope itemtype=\"http://schema.org/category\" itemprop=\"name\" value=\"automatic\">Automatic</option>-->\n    <!--<option itemscope itemtype=\"http://schema.org/category\" itemprop=\"name\" value=\"Mini-bus\">Mini buses</option>-->\n    <!--<option itemscope itemtype=\"http://schema.org/category\" itemprop=\"name\" value=\"Convertible\">Cabrio</option>-->\n    <!--<option value=\"\" selected=\"selected\">Show all</option>-->\n  <!--</select></div>-->\n  <!--<div class=\"fluid\">-->\n    <!--<br/>-->\n    <!--<button type=\"submit\"-->\n            <!--class=\"btn btn-primary pull-right searchPrice\"> Search for prices-->\n    <!--</button>-->\n  <!--</div>-->\n<!--</div>-->\n"

/***/ }),

/***/ "../../../../../src/app/main/main.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Reservation__ = __webpack_require__("../../../../../src/app/Reservation.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_moment__ = __webpack_require__("../../../../moment/moment.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__locations_service__ = __webpack_require__("../../../../../src/app/locations.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MainComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var MainComponent = (function () {
    function MainComponent(locationService) {
        this.locationService = locationService;
        this.title = 'LibertyCars';
        this.reservation = new __WEBPACK_IMPORTED_MODULE_1__Reservation__["a" /* Reservation */]();
    }
    MainComponent.prototype.ngOnInit = function () {
        this.reservation.datePickUp = __WEBPACK_IMPORTED_MODULE_2_moment__().format('YYYY-MM-DD');
        this.reservation.dateReturn = __WEBPACK_IMPORTED_MODULE_2_moment__().add(5, 'days').format('YYYY-MM-DD');
        this.returnTime = 1200;
        this.pickUpTime = 1200;
        this.getLocations();
        this.selectedPickUp = this.locations[0];
        this.selectedReturn = this.locations[0];
    };
    MainComponent.prototype.getLocations = function () {
        this.locations = this.locationService.getLocations();
    };
    return MainComponent;
}());
MainComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_14" /* Component */])({
        selector: 'app-main',
        template: __webpack_require__("../../../../../src/app/main/main.component.html"),
        styles: [__webpack_require__("../../../../../src/app/main/main.component.css")],
        providers: [__WEBPACK_IMPORTED_MODULE_3__locations_service__["a" /* LocationsService */]]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__locations_service__["a" /* LocationsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__locations_service__["a" /* LocationsService */]) === "function" && _a || Object])
], MainComponent);

var _a;
//# sourceMappingURL=main.component.js.map

/***/ }),

/***/ "../../../../../src/app/mock-cars.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CARS; });
var CARS = [
    {
        'id': 'A',
        'imageUrl': 'img/cars/seicento.jpg',
        'model': 'Fiat Seicento',
        'motor': '1100',
        'type': 'Manual',
        'seats': 4,
        'luggage': 2,
        'doors': 2,
        'ac': '-',
        'group': 'A'
    },
    {
        'id': 'B',
        'imageUrl': 'img/cars/i10.jpg',
        'model': 'Hyundai i10, Fiat Panda, Toyota Aygo',
        'motor': '1000',
        'type': 'Manual',
        'seats': 5,
        'luggage': 2,
        'doors': 4,
        'ac': 'Air Conditioned',
        'group': 'B'
    },
    {
        'id': 'C',
        'imageUrl': 'img/cars/up.jpg',
        'model': 'Toyota Yaris 1.1, Citroen C3, VW UP, Nissan Micra',
        'motor': '1200',
        'type': 'Manual',
        'seats': 5,
        'luggage': 3,
        'doors': 5,
        'ac': 'Air Conditioned',
        'group': 'C'
    },
    {
        'id': 'D',
        'imageUrl': 'img/cars/opel-corsa.jpg',
        'model': 'Opel Corsa, Peugeot 207, Fiat Grande Punto',
        'motor': '1200',
        'type': 'Manual',
        'seats': 5,
        'luggage': 4,
        'doors': 5,
        'ac': 'Air Conditioned',
        'group': 'D'
    },
    {
        'id': 'accent',
        'imageUrl': 'img/cars/accent-ac.jpg',
        'model': 'Hyundai Accent, Yaris',
        'motor': '1300',
        'type': 'Manual',
        'seats': 5,
        'luggage': 4,
        'doors': 4,
        'ac': 'Air Conditioned',
        'group': 'E'
    },
    {
        'id': 'jimny',
        'imageUrl': 'img/cars/suzuki-jimmy.jpg',
        'model': 'Suzuki Jimny',
        'motor': '1300',
        'type': '4x4',
        'seats': 4,
        'luggage': 4,
        'doors': 4,
        'ac': '-',
        'group': 'H'
    },
    {
        'id': 'multipla',
        'imageUrl': 'img/cars/multipla.jpg',
        'model': 'Fiat Multipla/Doblo 1.6',
        'motor': '1600',
        'type': 'Mini-bus',
        'seats': 7,
        'luggage': 6,
        'doors': 5,
        'ac': 'Air Conditioned',
        'group': 'K'
    },
    {
        'id': 'scudo',
        'imageUrl': 'img/cars/vivaro.jpg',
        'model': 'Fiat Scudo, Opel Vivaro, Opel Zafira ',
        'motor': '2000',
        'type': 'Mini-bus',
        'seats': 9,
        'luggage': 8,
        'doors': 5,
        'ac': 'Air Conditioned',
        'group': 'L'
    },
    {
        'id': 'smart',
        'imageUrl': 'img/cars/smart.jpg',
        'model': 'Smart Fortwo',
        'motor': '1000',
        'type': 'Manual',
        'seats': 2,
        'luggage': 1,
        'doors': 2,
        'ac': 'Air Conditioned',
        'group': 'M1'
    },
    {
        'id': 'corolla',
        'imageUrl': 'img/cars/corolla.jpg',
        'model': 'Toyota Corolla',
        'motor': '1600',
        'type': 'Manual',
        'seats': 5,
        'luggage': 5,
        'doors': 5,
        'ac': 'Air Conditioned',
        'group': 'M2'
    }
];
//# sourceMappingURL=mock-cars.js.map

/***/ }),

/***/ "../../../../../src/app/mock-locations.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LOCATIONS; });
var LOCATIONS = [
    {
        'id': 'Heraklion_Airport',
        'name': 'Heraklion Airport',
        'charge': 0
    },
    {
        'id': 'Heraklion_Port',
        'name': 'Heraklion Port',
        'charge': 0
    },
    {
        'id': 'Heraklion_Downtown',
        'name': 'Heraklion',
        'charge': 0
    },
    {
        'id': 'Agia_Pelagia',
        'name': 'Agia Pelagia',
        'charge': 0
    },
    {
        'id': 'Fodele',
        'name': 'Fodele',
        'charge': 0
    },
    {
        'id': 'Kokkini_Hani',
        'name': 'Kokkini Hani',
        'charge': 0
    },
    {
        'id': 'Gournes',
        'name': 'Gournes',
        'charge': 0
    },
    {
        'id': 'Gouves',
        'name': 'Gouves',
        'charge': 0
    },
    {
        'id': 'Analipsi',
        'name': 'Analipsi',
        'charge': 0
    },
    {
        'id': 'Anissaras',
        'name': 'Anissaras',
        'charge': 0
    },
    {
        'id': 'Chersonissos',
        'name': 'Chersonissos',
        'charge': 0
    },
    {
        'id': 'Koutouloufari',
        'name': 'Koutouloufari',
        'charge': 0
    },
    {
        'id': 'Piskopiano',
        'name': 'Piskopiano',
        'charge': 0
    },
    {
        'id': 'Stalis',
        'name': 'Stalis',
        'charge': 0
    },
    {
        'id': 'Malia',
        'name': 'Malia',
        'charge': 0
    },
    {
        'id': 'Matala',
        'name': 'Matala',
        'charge': 0
    },
    {
        'id': 'Rethymno_Port',
        'name': 'Rethymno Port',
        'charge': 0
    },
    {
        'id': 'Rethymno_Downtown',
        'name': 'Rethymno',
        'charge': 0
    },
    {
        'id': 'Scaleta',
        'name': 'Scaleta',
        'charge': 0
    },
    {
        'id': 'Panormo',
        'name': 'Panormo',
        'charge': 0
    },
    {
        'id': 'Kavros',
        'name': 'Kavros',
        'charge': 0
    },
    {
        'id': 'Bali',
        'name': 'Bali',
        'charge': 0
    },
    {
        'id': 'Agia_Marina',
        'name': 'Agia Marina',
        'charge': 0
    },
    {
        'id': 'Platanias',
        'name': 'Platanias',
        'charge': 0
    },
    {
        'id': 'Sitia',
        'name': 'Sitia',
        'charge': 0
    },
    {
        'id': 'Ierapetra',
        'name': 'Ierapetra',
        'charge': 0
    },
    {
        'id': 'Sisi',
        'name': 'Sisi',
        'charge': 0
    },
    {
        'id': 'Agios_Nikolaos',
        'name': 'Agios Nikolaos',
        'charge': 0
    },
    {
        'id': 'Agios_Nikolaos_Port',
        'name': 'Agios Nikolaos Port',
        'charge': 0
    },
    {
        'id': 'Platanias',
        'name': 'Platanias',
        'charge': 0
    },
    {
        'id': 'Chania_Port',
        'name': 'Chania Port',
        'charge': 0
    },
    {
        'id': 'Chania',
        'name': 'Chania',
        'charge': 0
    },
    {
        'id': 'Chania_Airport',
        'name': 'Chania Airport',
        'charge': 0
    },
    {
        'id': 'Ammoudara',
        'name': 'Ammoudara',
        'charge': 0
    },
    {
        'id': 'Lygaria',
        'name': 'Lygaria',
        'charge': 0
    }
];
//# sourceMappingURL=mock-locations.js.map

/***/ }),

/***/ "../../../../../src/app/not-found404/not-found404.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/not-found404/not-found404.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  not-found404 works!\n</p>\n"

/***/ }),

/***/ "../../../../../src/app/not-found404/not-found404.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotFound404Component; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var NotFound404Component = (function () {
    function NotFound404Component() {
    }
    NotFound404Component.prototype.ngOnInit = function () {
    };
    return NotFound404Component;
}());
NotFound404Component = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_14" /* Component */])({
        selector: 'app-not-found404',
        template: __webpack_require__("../../../../../src/app/not-found404/not-found404.component.html"),
        styles: [__webpack_require__("../../../../../src/app/not-found404/not-found404.component.css")]
    }),
    __metadata("design:paramtypes", [])
], NotFound404Component);

//# sourceMappingURL=not-found404.component.js.map

/***/ }),

/***/ "../../../../../src/app/privacy/privacy.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/privacy/privacy.component.html":
/***/ (function(module, exports) {

module.exports = "\n<div class=\"container col-9\">\n  <div class=\"row\">\n    <h2>Privacy policy</h2>\n\n    <p>In these Terms of Use, \"we\", \"our\" and \"us\" means {{company}} and \"you\" and \"your\" means any person who\n      accesses and uses this Website.</p>\n\n    <p> This policy sets out the basis of what information and personal data we collect from you. Please read the following\n      carefully in order to understand how we store your information and treat it.</p>\n\n    <p><b>Information we may collect for you </b> </p>\n    <p> The information which is collected from you includes any information you provide by filling any fields  within\n      this Website or site materials. This information includes the booking information which is required in order to book a product or service.</p>\n    <p> Details of the transactions which occur in this site.</p>\n    <p> Information provided during the bookings process include name, address, date of birth, itinerary information, rental charges,\n      deposit amount, car preference and rental company. </p>\n\n\n    <p><b>Cookies</b></p>\n    <p> {{company}} uses cookies in order to make the use of this website as efficient and fast as possible in order to provide\n      the best user experience to you. </p>\n\n    <p><b>Where we store your data</b></p>\n    <p> Your information is stored and located within the European Economic Area(EEA).</p>\n\n    <p><b>How secure if my information</b></p>\n    <p>We work to protect the security of your information during transmission by using SSL,\n      which encrypts information you input.</p>\n    <p> We do not store any credit card information you provide us.</p>\n    <p>We maintain physical, electronic and procedural safeguards in connection with the collection, storage and disclosure of personally identifiable customer information.</p>\n    <p><b>Uses of your data</b></p>\n\n    <p>To facilitate vehicle rental bookings  by disclosing personal information to the    vehicle rental provider when you\n      book the vehicle rental on this website</p>\n    <p> To carry out any obligations that might arrise between you and us</p>\n    <p>To notify you for any changes to our service.</p>\n    <p>Information about our customers is an important part of our business and we are not in the business of selling it to others.</p>\n\n    <p><b>Changes</b></p>\n    <p>\n      {{company}} reserves the right to change the Privacy Terms and the contents of this booking engine for any reason\n      and without prior notice and without liability to you, any other user or any third party.</p>\n    <p><b>Your rights</b></p>\n    <p>\n      Under the Data Protection Act 1998 you have certain rights. For example, in accordance with the Data Protection Act\n      1998 we will always let you have a copy of the personal information we have about you, if you request it from us in\n      writing. The law allows us to charge you a £10 fee for a copy of such information and we may do so. </p>\n\n    <p><b>Contact</b></p>\n    <p>Our aim is at all times to provide you with an excellent service.\n      If you are unhappy with any product you have obtained from a third party or have any complaint regarding any third\n      party, you should address your complaint directly to that third party. If you require their contact details, please\n      contact us at info@{{company}}.</p>\n    <p>All rights in {{company}} are owned by us.</p>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/privacy/privacy.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PrivacyComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PrivacyComponent = (function () {
    function PrivacyComponent() {
        this.company = 'LibertyCars.com';
    }
    PrivacyComponent.prototype.ngOnInit = function () {
    };
    return PrivacyComponent;
}());
PrivacyComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_14" /* Component */])({
        selector: 'app-privacy',
        template: __webpack_require__("../../../../../src/app/privacy/privacy.component.html"),
        styles: [__webpack_require__("../../../../../src/app/privacy/privacy.component.css")]
    }),
    __metadata("design:paramtypes", [])
], PrivacyComponent);

//# sourceMappingURL=privacy.component.js.map

/***/ }),

/***/ "../../../../../src/app/search/search.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".search {\n  background-color: #001f3f;\n  color: #FBFBFB;\n  height: 4%;\n  vertical-align: middle;\n  text-align: center; /* FF1+ */ /* Saf3-4 */\n  border-radius: 12px; /* Opera 10.5, IE 9, Saf5, Chrome */\n}\n\n\n.iconsInfo span {\n  font-size: 18px;\n  line-height: 18px;\n  width: 80%;\n  font-family: \"Helvetica Neue\", Helvetica, Arial, sans-serif;\n  display: block;\n  color: #666;\n  /*background: url(\"../img/features-bg-14.png\") no-repeat transparent;*/\n  padding: 0px 0px 0px 27px;\n  margin: 2px 0px;\n  border-bottom: 1px solid #ddd;\n  font-weight: 300;\n}\n\n.carList {\n  background-color: aliceblue;\n  height: 4%;\n  vertical-align: middle;\n  text-align: center; /* FF1+ */ /* Saf3-4 */\n  border-radius: 12px; /* Opera 10.5, IE 9, Saf5, Chrome */\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/search/search.component.html":
/***/ (function(module, exports) {

module.exports = "\n<div class=\"container search\">\n  <div class=\"row mt-3\">\n    <div class=\"container mt-2\">\n\n      <div class=\"row col-sm-12 d-flex justify-content-center-sm justify-content-end\">\n        <div class=\"col-xs-3 col-sm-2\" style=\"width: auto\">\n          <label for=\"pickupAddress\" class=\"\">Pick up:</label>\n        </div>\n        <div class=\"col-xs col-sm-3\" style=\"width: auto\">\n          <select class=\" custom-select mx-sm-3\" id=\"pickupAddress\" [(ngModel)]=\"reservation.pickUpFrom\" [ngModelOptions]=\"{standalone: true}\">\n            <option *ngFor='let location of locations' [ngValue]='location'>{{location.name}}</option>\n          </select>\n        </div>\n        <div class=\"col-xs col-sm-3\" style=\"width: 60%\">\n          <input class=\"form-control col-11\" [ngModel]=\"reservation.datePickUp | date:'yyyy-MM-dd'\" (ngModelChange)=\"reservation.datePickUp = $event\" type=\"date\" name=\"date-pickup\"/>\n        </div>\n        <div class=\"col-xs col-sm-2\" style=\"width: 40%\">\n          <select class=\"form-control custom-select\" id=\"pickupTime\" [(ngModel)]=\"reservation.pickUpTime\" [ngModelOptions]=\"{standalone: true}\">\n            <option [ngValue]=\"1000\">10:00</option>\n            <option [ngValue]=\"1030\">10:30</option>\n            <option [ngValue]=\"1100\">11:00</option>\n            <option [ngValue]=\"1130\">11:30</option>\n            <option [ngValue]=\"1200\">12:00</option>\n            <option [ngValue]=\"1230\">12:30</option>\n            <option [ngValue]=\"1300\">13:00</option>\n          </select>\n        </div>\n        <div class=\"col col-2\"></div>\n      </div>\n\n\n    </div>\n  </div>\n  <div class=\"row mt-1 mb-2\">\n    <div class=\"container\">\n\n      <div class=\"row col-sm-12 d-flex justify-content-center-sm justify-content-end\">\n        <div class=\"col-xs-3 col-sm-2\" style=\"width: auto\">\n          <label for=\"pickupAddress\" class=\"\">Return:</label>\n        </div>\n        <div class=\"col-xs col-sm-3\" style=\"width: auto\">\n          <select class=\" custom-select mx-sm-3\" id=\"returnAddress\" [(ngModel)]=\"reservation.returnTo\" [ngModelOptions]=\"{standalone: true}\">\n            <option *ngFor='let location of locations' [ngValue]='location'>{{location.name}}</option>\n          </select>\n        </div>\n        <div class=\"col-xs col-sm-3\" style=\"width: 60%\">\n          <input class=\"form-control col-11\" [ngModel]=\"reservation.dateReturn | date:'yyyy-MM-dd'\" (ngModelChange)=\"reservation.dateReturn = $event\" type=\"date\" name=\"date-return\"/>\n\n        </div>\n        <div class=\"col-xs col-sm-2\" style=\"width: 40%\">\n          <select class=\"form-control custom-select\" id=\"returnTime\" [(ngModel)]=\"reservation.returnTime\" [ngModelOptions]=\"{standalone: true}\">\n            <option [ngValue]=\"1000\">10:00</option>\n            <option [ngValue]=\"1030\">10:30</option>\n            <option [ngValue]=\"1100\">11:00</option>\n            <option [ngValue]=\"1130\">11:30</option>\n            <option [ngValue]=\"1200\" selected>12:00</option>\n            <option [ngValue]=\"1230\">12:30</option>\n            <option [ngValue]=\"1300\">13:00</option>\n          </select>\n        </div>\n          <div class=\"col-sm-2 col-xs-6\">\n            <button type=\"submit\" class=\"btn btn-primary\"\n                    [routerLink]=\"['/search', {\n                    dtr: reservation.dateReturn ,\n                    dpu: reservation.datePickUp,\n                    pl: reservation.pickUpFrom.id,\n                    rl: reservation.returnTo.id,\n                    pT: reservation.pickUpTime,\n                    rT: reservation.returnTime}\n                   ]\" routerLinkActive=\"active\"> Search for prices </button>\n          </div>\n      </div>\n\n\n    </div>\n  </div>\n  <div class=\"row\"> </div>\n</div>\n\n<div class=\"container mt-4\">\n  <div class=\"row\" >\n    <div class=\"container\">\n      <ol style=\"-webkit-padding-start: 0px;\">\n        <div *ngFor=\"let car of cars\" style=\"margin-bottom: 1%\" class=\"carList col-12\" >\n          <div class=\"row\">\n            <b style=\"margin-left: 2%\">{{car.model}}</b>\n          <span\n            popover=\"The car provided will be of similar engine power, passenger number, car type, category as selected option.\"\n            popover-trigger=\"mouseenter\">  or similar</span> <br> <span><img class=\"thumb\"> </span>\n          </div>\n\n          <div class=\"row mt-2\">\n            <div class=\"col-sx-12 col-sm-3\">\n              <img style=\"width:90%; height: 150px; margin-left: 1%\" src=\"assets/{{car.imageUrl}}\">\n            </div>\n            <div class=\"col-sx-12 col-sm-2\">\n            <p class=\"iconsInfo\">\n              <span class=\"motor\" >{{ car.motor }}cc</span>\n              <span class=\"doors\" >{{ car.doors }} Doors</span>\n              <span class=\"seats\" >{{ car.seats }} Seats</span>\n              <span class=\"airco\" > {{ car.ac }}</span>\n              <span class=\"trans\">{{car.type}}</span>\n              <span class=\"bags\" style=\"border-bottom:none;\">{{ car.luggage }} luggages</span>\n            </p>\n            </div>\n            <div class=\"col-sx-12 col-sm-4\">\n              <span >Final price &amp; no extra insurances.</span><br>\n              <span > Theft Cover with<b> no excess</b>.</span><br>\n              <span > Full Insurance (Collision Damage Waiver) with <strong>no excess</strong>.</span><br>\n              <span>  Personal Accident Insurance for passengers & drivers.</span><br>\n              <span>  FREE Road Assistance</span><br>\n\n            </div>\n            <div class=\"col-sx-12 col-sm-3\">\n              <h3 class=\"ccfont\" style=\"width: 99%; cursor: default;\"> Price :  &pound; 1000 </h3>\n              <br>\n              <button type=\"submit\" class=\"btn btn-primary\" style=\"width: 60%;\"\n                      [routerLink]=\"['/book',\n                        { id: car.id, dtr: reservation.dateReturn ,\n                          dpu: reservation.datePickUp,\n                          pl: reservation.pickUpFrom.id,\n                          rl: reservation.returnTo.id,\n                          pT: reservation.pickUpTime,\n                          rT: reservation.returnTime\n                          }]\"\n                      routerLinkActive=\"active\">\n                Book\n              </button>\n            </div>\n          </div>\n        </div>\n        </ol>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/search/search.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_switchMap__ = __webpack_require__("../../../../rxjs/add/operator/switchMap.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_switchMap___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_switchMap__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__cars_service__ = __webpack_require__("../../../../../src/app/cars.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Reservation__ = __webpack_require__("../../../../../src/app/Reservation.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__locations_service__ = __webpack_require__("../../../../../src/app/locations.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var SearchComponent = (function () {
    function SearchComponent(carsService, locationService, route) {
        this.carsService = carsService;
        this.locationService = locationService;
        this.route = route;
        this.reservation = new __WEBPACK_IMPORTED_MODULE_3__Reservation__["a" /* Reservation */]();
    }
    SearchComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.paramMap.subscribe(function (params) { return _this.setReservation(params.get('dtr'), params.get('dpu'), params.get('pl'), params.get('rl'), params.get('pT'), params.get('rT')); });
        this.getCars();
        this.getLocations();
    };
    SearchComponent.prototype.getLocations = function () {
        this.locations = this.locationService.getLocations();
    };
    SearchComponent.prototype.setReservation = function (dateReturn, datePickUp, pickUpID, returnID, pickUpTime, returnTime) {
        this.reservation.dateReturn = dateReturn;
        this.reservation.datePickUp = datePickUp;
        this.reservation.pickUpFrom = this.locationService.getLocation(pickUpID);
        this.reservation.returnTo = this.locationService.getLocation(returnID);
        this.reservation.pickUpTime = +pickUpTime;
        this.reservation.returnTime = +returnTime;
    };
    SearchComponent.prototype.getCars = function () {
        var _this = this;
        this.carsService.getCars().then(function (cars) { return _this.cars = cars; });
    };
    return SearchComponent;
}());
SearchComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["_14" /* Component */])({
        selector: 'app-search',
        template: __webpack_require__("../../../../../src/app/search/search.component.html"),
        styles: [__webpack_require__("../../../../../src/app/search/search.component.css")],
        providers: [__WEBPACK_IMPORTED_MODULE_2__cars_service__["a" /* CarsService */], __WEBPACK_IMPORTED_MODULE_5__locations_service__["a" /* LocationsService */]]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__cars_service__["a" /* CarsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__cars_service__["a" /* CarsService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_5__locations_service__["a" /* LocationsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__locations_service__["a" /* LocationsService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_4__angular_router__["b" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__angular_router__["b" /* ActivatedRoute */]) === "function" && _c || Object])
], SearchComponent);

var _a, _b, _c;
//# sourceMappingURL=search.component.js.map

/***/ }),

/***/ "../../../../../src/app/terms/terms.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/terms/terms.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container col-9\">\n  <div class=\"row\">\n\n    <h2>IMPORTANT LEGAL NOTICE</h2>\n\n    <p>In these Terms of Use, \"we\", \"our\" and \"us\" means {{company}} and \"you\" and \"your\" means any person who\n      accesses and uses this Website.</p>\n\n    <p>You pay a booking deposit at the time of making your booking and the remaining of price is paid to the rental vehicle provider\n      on arrival.</p>\n\n    <p> Access to and use of this Website is subject to these Terms of Use and our Privacy Policy. By accessing and using\n      this Website you agree to be bound by and to act in accordance with these Terms of Use and our Privacy Policy. If\n      you do not agree to these Terms of Use or our Privacy Policy, you are not permitted to access and use this Website\n      and you should cease such access and/or use immediately.</p>\n\n    <p><b>Children </b> </p>\n    <p>We do not sell products for purchase by children. We sell children's products for purchase by\n      adults. If you are under 18 you may use our services only with the involvement of a parent or guardian.\n    </p>\n    <p>You must be at least 13 years of age to use this website. If you are not at least 13 years old, you must not access\n      or use this website.</p>\n\n    <p><b> Use of Third Party Services.</b></p>\n    <p> When you use the {{company}}, you may also be using the services of one\n      or more third parties, such as a wireless carrier, a mobile platform provider or a Internet provider. Your use of these third party\n      services may be subject to the separate policies, terms of use, and fees of these third parties. </p>\n\n    <p>We provide an independent online services which enables you to research and compare car rental\n      services provided by third parties whilst using this Website</p>\n\n    <p>You can apply for a number of products and services via our Website. These products and services are not provided by\n      us but are instead provided by third parties over whom we do not have control. It is your responsibility to satisfy\n      yourself that you wish to obtain any product or services before doing so. We are not responsible or liable for any\n      loss or damage you may suffer or incur in connection with any product or services you obtain after using this Website\n      or for any acts, omissions, errors or defaults of any third party in connection with that product or service.</p>\n\n    <p> If you apply for and obtain any product or services, you will be contracting with a third party who will be providing\n      that product or services to you on their own terms and conditions. It is your responsibility to ensure that you\n      understand and agree with those terms and conditions before entering into a contract to obtain that product or\n      service. We are not responsible or liable for any loss or damage you may suffer or incur in connection with the\n      terms and conditions applying to any contract entered into by you with any third party in relation to any product or\n      services or for any acts, omissions, errors or defaults of any third party in connection with those terms and\n      conditions</p>\n\n    <p>Please be aware that whilst this Website provides information on a wide range of products or services, there may be\n      other products or services available on the market which are not shown on this Website and which may be more\n      appropriate or suitable for you than those shown on this Website</p>\n\n    <p>Please be aware that the information and descriptions of products and services on this Website may not represent the\n      complete descriptions of all the features and terms and conditions of those products and services. You must ensure\n      that you carefully read all the features and terms and conditions of any product or services before applying for\n      it.</p>\n\n    <p>Any views, opinions, advice or assistance which is given or provided to you by a third party after you have used this\n      Website do not represent our views, opinions, advice or assistance and are not checked, monitored, reviewed,\n      verified or endorsed by us. We do not endorse, recommend or take responsibility for any third party who provides you\n      with any views, opinions advice or assistance. You act or refrain from acting on any third party's views, opinions,\n      advice or assistance at your sole risk and sole discretion and you are solely responsible for any decision to act or\n      refrain from acting on such views, opinions, advice or assistance. We are not responsible or liable for any loss or\n      damage you may suffer or incur in connection with such views, opinions, advice or assistance including in relation\n      to their accuracy, truthfulness or completeness or for any acts, omissions, errors or defaults of any third party in\n      connection with such views, opinions, advice or assistance.</p>\n\n\n    <p><b>Fees and Charges</b></p>\n    <p>\n      The credit card/administration charge if any which {{company}}  charges you is non-refundable in all circumstances.\n      It is the responsibility of the rental vehicle provider to process your reservation correctly. The accuracy of any\n      quote for renting a vehicle provided by this booking engine and any booking made on this booking engine depends on\n      the details you provide on this booking engine.</p>\n\n    <p>You agree to pay any fees and/or other charges incurred by you in accordance with any payment provisions set out on\n      this Website from time to time.</p>\n\n    <p> <b>Making Reservations and Purchasing or Requesting Products or Services.</b></p>\n    <p> If you wish to make reservations or to purchase or request products or services described on this website, you may be asked by us\n      to supply certain data applicable to your reservation or purchase, including, without\n      limitation, credit card information and other personally identifiable data about you. You understand that any such\n      personally identifiable data will be treated by us in the manner described in the privacy policy accessible via a\n      link provided at the bottom of the homepage of this website and, such\n      data will be treated by such provider in the manner set forth in that provider's privacy statement; you acknowledge\n      that we are not responsible for the information collection or privacy practices of these providers or any other\n      third parties. You agree that all data that you provide in making reservations or purchases or requesting services\n      will be accurate, current, and complete. You agree to pay all charges incurred by you or any users of your account\n      and credit card or other payment mechanism at the rate(s) or price(s) in effect when such charges are incurred. You\n      will also be responsible for paying any applicable taxes relating to your purchases.\n    </p>\n\n    <p>Please be aware that nothing on this Website is, or shall be deemed to constitute, an offer by us or any third party\n      to sell to you any product or services or to enter into any contract with you in respect of any product or service.\n      By submitting your details, you are making an offer to obtain the relevant product or services from the relevant\n      third party on its terms and conditions that may be accepted or rejected. The contract for the product or services\n      will only be concluded once your offer has been accepted. Please note that acceptance of your payment where possible\n      on this Website does not constitute acceptance of your offer. You will receive confirmation when your offer has been\n      accepted.</p>\n\n\n    <p><b>Disclaimers.</b></p>\n    <p>\n      The information, site materials, any product or services and the website and its connection are provided on an \"as is\"\n      \"as available\" basis without representations or warranties of any kind, either express or implied, unless specifically provided\n      in a written agreement with us.\n      We do not warrant that the use of this website or its materials will be error-free, secure, uninterrupted, free of any sort\n      of viruses or harmful components.\n      You acknowledge that you are responsible for obtaining or maintaining all computer hardware and other equipment needed to access\n      and use this website, and all charges related thereto. No opinion, advice or statement made on this website or by any of our employees or visitors\n      on this website, or otherwise, shall create any warranty, unless otherwise expressly provided in a  written agreement\n      with us which you are a party with regard to the services or product.\n      Your use of this website is entirely at your own risk.\n    </p>\n\n    <p><b> Conflicts. </b></p>\n    <p>In the event a conflict arises between any information posted on this website, this Agreement,\n      and/or any contract you have with us, the terms of your contract with us will prevail over this Agreement and the\n      information posted on this website, and this Agreement will prevail over information posted on this website.\n    </p>\n\n    <p><b>Limitation of Liability.</b></p>\n    <p>\n      As a condition of your use of this website you agree that neither we, nor any of our affiliates, suppliers, employees, will\n      be liable to you or any third party for any direct, indirect, or other damages under any contract,\n      negligence, strict liability or other, or consequential loss of business opportunities, lost data, interrupted communications, damages,\n      expense, or costs resulting directly or indirectly for or other connections with: this website, including, but not limited to,\n      damages resulting or arising from this website or hyperlinked through this website, or mistakes, errors, defects, delays, interruptions,\n      eavesdropping by third parties, or any failure of performance of this website, strikes, war, any natural disaster,\n      floods, fire, earthquakes, power failures,large increases of online activity (spikes), viruses,\n      hardware failures, cyber attacks, or anything that limits the usage or access of this website;\n      or loss of security or interception from third parties of information you have provided while using this website.\n\n      If you are dissatisfied with this website and/or site material from within this website you should stop using this website\n      and/or its materials, as applicable.\n    </p>\n\n    <p>This site offers links to other third party sites thereby enabling you to leave this site and go directly to the linked site.\n      We are not responsible for the content of any linked site or any link in a linked site.</p>\n\n    <p><b>Exclusions of liability</b></p>\n    <p>Nothing in these terms and conditions excludes or limits our liability for death or\n      personal injury caused by our negligence or for our fraud, or excludes or limits our duties or any liability under\n      the Financial Services and Markets Act 2000, as amended, (\"FSMA\") or any conduct of business rules developed\n      pursuant to FSMA.</p>\n\n    <p>We are not responsible or liable for any indirect losses or damages suffered or incurred by you or for any losses or\n      damages suffered or incurred by you which were not foreseeable by us when you accessed or used the Website.</p>\n\n    <p><b>WAIVER</b></p>\n    <p>\n\n      Our failure to enforce at any time or for any period any one or more of the Terms of Use shall not be a waiver of\n      them or the rights attaching to any of them.</p>\n\n    <p>\n      <b> Modification of the terms of use</b>\n    </p>\n    <p>\n      {{company}}  reserves the right to change the Terms of Use and the contents of this booking engine for any reason\n      and without prior notice and without liability to you, any other user or any third party. This right shall not\n      affect the Terms of Use accepted by you, the user, upon making a legitimate booking or purchase using this booking\n      engine. You should check these Terms of Use for any changes each time you access the booking engine. The rental\n      vehicle provider may from time to time change its terms and conditions (which are available on this booking engine)\n      upon which the rental vehicle provider makes the reserved vehicle available to you. {{company}}  is not responsible\n      and shall have no liability if the rental vehicle provider's terms and conditions available on this booking engine\n      have been or are changed by the rental vehicle provider.</p>\n\n    <p> If any provision of these Terms of Use is held to be unlawful, invalid or unenforceable, that provision shall be\n      deemed deleted from these Terms of Use and the validity and enforceability of the remaining provisions of these\n      Terms of Use shall not be affected.</p>\n\n    <p>These Terms of Use and your access to and use of this Website shall be governed by and interpreted in accordance with\n      English law.</p>\n\n    <p><b>Your Rights</b></p>\n    <p>\n      You have certain rights under the Data Protection Act 1998. For example, in accordance with the Data Protection Act\n      1998 we will always let you have a copy of the personal information we have about you, if you request it from us in\n      writing. The law allows us to charge you a £10 fee for a copy of such information and we may do so. </p>\n\n    <p><b>Complaints</b></p>\n\n    <p>Our aim is at all times to provide you with an excellent service.\n      If you are unhappy with any product you have obtained from a third party or have any complaint regarding any third\n      party, you should address your complaint directly to that third party. If you require their contact details, please\n      contact us at info@{{company}} .</p>\n    <p>All rights in {{company}}  are owned by us.</p>\n\n    <p>If you have any concern about the terms of {{company}} , please <a style=\"color:blue;\"\n                                                                          href=\"mailto:info@{{company}}?Subject=TermsQuestions\"> e-mail us </a>with a thorough description and we\n      will try to resolve the issue for you.</p>\n\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/terms/terms.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TermsComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TermsComponent = (function () {
    function TermsComponent() {
        this.company = 'LibertyCars.com';
    }
    TermsComponent.prototype.ngOnInit = function () {
    };
    return TermsComponent;
}());
TermsComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_14" /* Component */])({
        selector: 'app-terms',
        template: __webpack_require__("../../../../../src/app/terms/terms.component.html"),
        styles: [__webpack_require__("../../../../../src/app/terms/terms.component.css")]
    }),
    __metadata("design:paramtypes", [])
], TermsComponent);

//# sourceMappingURL=terms.component.js.map

/***/ }),

/***/ "../../../../../src/assets/hat.jpg":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "hat.724aa399cd5cfddfe919.jpg";

/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/@angular/platform-browser-dynamic.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["a" /* enableProdMode */])();
}
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ "../../../../moment/locale recursive ^\\.\\/.*$":
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "../../../../moment/locale/af.js",
	"./af.js": "../../../../moment/locale/af.js",
	"./ar": "../../../../moment/locale/ar.js",
	"./ar-dz": "../../../../moment/locale/ar-dz.js",
	"./ar-dz.js": "../../../../moment/locale/ar-dz.js",
	"./ar-kw": "../../../../moment/locale/ar-kw.js",
	"./ar-kw.js": "../../../../moment/locale/ar-kw.js",
	"./ar-ly": "../../../../moment/locale/ar-ly.js",
	"./ar-ly.js": "../../../../moment/locale/ar-ly.js",
	"./ar-ma": "../../../../moment/locale/ar-ma.js",
	"./ar-ma.js": "../../../../moment/locale/ar-ma.js",
	"./ar-sa": "../../../../moment/locale/ar-sa.js",
	"./ar-sa.js": "../../../../moment/locale/ar-sa.js",
	"./ar-tn": "../../../../moment/locale/ar-tn.js",
	"./ar-tn.js": "../../../../moment/locale/ar-tn.js",
	"./ar.js": "../../../../moment/locale/ar.js",
	"./az": "../../../../moment/locale/az.js",
	"./az.js": "../../../../moment/locale/az.js",
	"./be": "../../../../moment/locale/be.js",
	"./be.js": "../../../../moment/locale/be.js",
	"./bg": "../../../../moment/locale/bg.js",
	"./bg.js": "../../../../moment/locale/bg.js",
	"./bn": "../../../../moment/locale/bn.js",
	"./bn.js": "../../../../moment/locale/bn.js",
	"./bo": "../../../../moment/locale/bo.js",
	"./bo.js": "../../../../moment/locale/bo.js",
	"./br": "../../../../moment/locale/br.js",
	"./br.js": "../../../../moment/locale/br.js",
	"./bs": "../../../../moment/locale/bs.js",
	"./bs.js": "../../../../moment/locale/bs.js",
	"./ca": "../../../../moment/locale/ca.js",
	"./ca.js": "../../../../moment/locale/ca.js",
	"./cs": "../../../../moment/locale/cs.js",
	"./cs.js": "../../../../moment/locale/cs.js",
	"./cv": "../../../../moment/locale/cv.js",
	"./cv.js": "../../../../moment/locale/cv.js",
	"./cy": "../../../../moment/locale/cy.js",
	"./cy.js": "../../../../moment/locale/cy.js",
	"./da": "../../../../moment/locale/da.js",
	"./da.js": "../../../../moment/locale/da.js",
	"./de": "../../../../moment/locale/de.js",
	"./de-at": "../../../../moment/locale/de-at.js",
	"./de-at.js": "../../../../moment/locale/de-at.js",
	"./de-ch": "../../../../moment/locale/de-ch.js",
	"./de-ch.js": "../../../../moment/locale/de-ch.js",
	"./de.js": "../../../../moment/locale/de.js",
	"./dv": "../../../../moment/locale/dv.js",
	"./dv.js": "../../../../moment/locale/dv.js",
	"./el": "../../../../moment/locale/el.js",
	"./el.js": "../../../../moment/locale/el.js",
	"./en-au": "../../../../moment/locale/en-au.js",
	"./en-au.js": "../../../../moment/locale/en-au.js",
	"./en-ca": "../../../../moment/locale/en-ca.js",
	"./en-ca.js": "../../../../moment/locale/en-ca.js",
	"./en-gb": "../../../../moment/locale/en-gb.js",
	"./en-gb.js": "../../../../moment/locale/en-gb.js",
	"./en-ie": "../../../../moment/locale/en-ie.js",
	"./en-ie.js": "../../../../moment/locale/en-ie.js",
	"./en-nz": "../../../../moment/locale/en-nz.js",
	"./en-nz.js": "../../../../moment/locale/en-nz.js",
	"./eo": "../../../../moment/locale/eo.js",
	"./eo.js": "../../../../moment/locale/eo.js",
	"./es": "../../../../moment/locale/es.js",
	"./es-do": "../../../../moment/locale/es-do.js",
	"./es-do.js": "../../../../moment/locale/es-do.js",
	"./es.js": "../../../../moment/locale/es.js",
	"./et": "../../../../moment/locale/et.js",
	"./et.js": "../../../../moment/locale/et.js",
	"./eu": "../../../../moment/locale/eu.js",
	"./eu.js": "../../../../moment/locale/eu.js",
	"./fa": "../../../../moment/locale/fa.js",
	"./fa.js": "../../../../moment/locale/fa.js",
	"./fi": "../../../../moment/locale/fi.js",
	"./fi.js": "../../../../moment/locale/fi.js",
	"./fo": "../../../../moment/locale/fo.js",
	"./fo.js": "../../../../moment/locale/fo.js",
	"./fr": "../../../../moment/locale/fr.js",
	"./fr-ca": "../../../../moment/locale/fr-ca.js",
	"./fr-ca.js": "../../../../moment/locale/fr-ca.js",
	"./fr-ch": "../../../../moment/locale/fr-ch.js",
	"./fr-ch.js": "../../../../moment/locale/fr-ch.js",
	"./fr.js": "../../../../moment/locale/fr.js",
	"./fy": "../../../../moment/locale/fy.js",
	"./fy.js": "../../../../moment/locale/fy.js",
	"./gd": "../../../../moment/locale/gd.js",
	"./gd.js": "../../../../moment/locale/gd.js",
	"./gl": "../../../../moment/locale/gl.js",
	"./gl.js": "../../../../moment/locale/gl.js",
	"./gom-latn": "../../../../moment/locale/gom-latn.js",
	"./gom-latn.js": "../../../../moment/locale/gom-latn.js",
	"./he": "../../../../moment/locale/he.js",
	"./he.js": "../../../../moment/locale/he.js",
	"./hi": "../../../../moment/locale/hi.js",
	"./hi.js": "../../../../moment/locale/hi.js",
	"./hr": "../../../../moment/locale/hr.js",
	"./hr.js": "../../../../moment/locale/hr.js",
	"./hu": "../../../../moment/locale/hu.js",
	"./hu.js": "../../../../moment/locale/hu.js",
	"./hy-am": "../../../../moment/locale/hy-am.js",
	"./hy-am.js": "../../../../moment/locale/hy-am.js",
	"./id": "../../../../moment/locale/id.js",
	"./id.js": "../../../../moment/locale/id.js",
	"./is": "../../../../moment/locale/is.js",
	"./is.js": "../../../../moment/locale/is.js",
	"./it": "../../../../moment/locale/it.js",
	"./it.js": "../../../../moment/locale/it.js",
	"./ja": "../../../../moment/locale/ja.js",
	"./ja.js": "../../../../moment/locale/ja.js",
	"./jv": "../../../../moment/locale/jv.js",
	"./jv.js": "../../../../moment/locale/jv.js",
	"./ka": "../../../../moment/locale/ka.js",
	"./ka.js": "../../../../moment/locale/ka.js",
	"./kk": "../../../../moment/locale/kk.js",
	"./kk.js": "../../../../moment/locale/kk.js",
	"./km": "../../../../moment/locale/km.js",
	"./km.js": "../../../../moment/locale/km.js",
	"./kn": "../../../../moment/locale/kn.js",
	"./kn.js": "../../../../moment/locale/kn.js",
	"./ko": "../../../../moment/locale/ko.js",
	"./ko.js": "../../../../moment/locale/ko.js",
	"./ky": "../../../../moment/locale/ky.js",
	"./ky.js": "../../../../moment/locale/ky.js",
	"./lb": "../../../../moment/locale/lb.js",
	"./lb.js": "../../../../moment/locale/lb.js",
	"./lo": "../../../../moment/locale/lo.js",
	"./lo.js": "../../../../moment/locale/lo.js",
	"./lt": "../../../../moment/locale/lt.js",
	"./lt.js": "../../../../moment/locale/lt.js",
	"./lv": "../../../../moment/locale/lv.js",
	"./lv.js": "../../../../moment/locale/lv.js",
	"./me": "../../../../moment/locale/me.js",
	"./me.js": "../../../../moment/locale/me.js",
	"./mi": "../../../../moment/locale/mi.js",
	"./mi.js": "../../../../moment/locale/mi.js",
	"./mk": "../../../../moment/locale/mk.js",
	"./mk.js": "../../../../moment/locale/mk.js",
	"./ml": "../../../../moment/locale/ml.js",
	"./ml.js": "../../../../moment/locale/ml.js",
	"./mr": "../../../../moment/locale/mr.js",
	"./mr.js": "../../../../moment/locale/mr.js",
	"./ms": "../../../../moment/locale/ms.js",
	"./ms-my": "../../../../moment/locale/ms-my.js",
	"./ms-my.js": "../../../../moment/locale/ms-my.js",
	"./ms.js": "../../../../moment/locale/ms.js",
	"./my": "../../../../moment/locale/my.js",
	"./my.js": "../../../../moment/locale/my.js",
	"./nb": "../../../../moment/locale/nb.js",
	"./nb.js": "../../../../moment/locale/nb.js",
	"./ne": "../../../../moment/locale/ne.js",
	"./ne.js": "../../../../moment/locale/ne.js",
	"./nl": "../../../../moment/locale/nl.js",
	"./nl-be": "../../../../moment/locale/nl-be.js",
	"./nl-be.js": "../../../../moment/locale/nl-be.js",
	"./nl.js": "../../../../moment/locale/nl.js",
	"./nn": "../../../../moment/locale/nn.js",
	"./nn.js": "../../../../moment/locale/nn.js",
	"./pa-in": "../../../../moment/locale/pa-in.js",
	"./pa-in.js": "../../../../moment/locale/pa-in.js",
	"./pl": "../../../../moment/locale/pl.js",
	"./pl.js": "../../../../moment/locale/pl.js",
	"./pt": "../../../../moment/locale/pt.js",
	"./pt-br": "../../../../moment/locale/pt-br.js",
	"./pt-br.js": "../../../../moment/locale/pt-br.js",
	"./pt.js": "../../../../moment/locale/pt.js",
	"./ro": "../../../../moment/locale/ro.js",
	"./ro.js": "../../../../moment/locale/ro.js",
	"./ru": "../../../../moment/locale/ru.js",
	"./ru.js": "../../../../moment/locale/ru.js",
	"./sd": "../../../../moment/locale/sd.js",
	"./sd.js": "../../../../moment/locale/sd.js",
	"./se": "../../../../moment/locale/se.js",
	"./se.js": "../../../../moment/locale/se.js",
	"./si": "../../../../moment/locale/si.js",
	"./si.js": "../../../../moment/locale/si.js",
	"./sk": "../../../../moment/locale/sk.js",
	"./sk.js": "../../../../moment/locale/sk.js",
	"./sl": "../../../../moment/locale/sl.js",
	"./sl.js": "../../../../moment/locale/sl.js",
	"./sq": "../../../../moment/locale/sq.js",
	"./sq.js": "../../../../moment/locale/sq.js",
	"./sr": "../../../../moment/locale/sr.js",
	"./sr-cyrl": "../../../../moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "../../../../moment/locale/sr-cyrl.js",
	"./sr.js": "../../../../moment/locale/sr.js",
	"./ss": "../../../../moment/locale/ss.js",
	"./ss.js": "../../../../moment/locale/ss.js",
	"./sv": "../../../../moment/locale/sv.js",
	"./sv.js": "../../../../moment/locale/sv.js",
	"./sw": "../../../../moment/locale/sw.js",
	"./sw.js": "../../../../moment/locale/sw.js",
	"./ta": "../../../../moment/locale/ta.js",
	"./ta.js": "../../../../moment/locale/ta.js",
	"./te": "../../../../moment/locale/te.js",
	"./te.js": "../../../../moment/locale/te.js",
	"./tet": "../../../../moment/locale/tet.js",
	"./tet.js": "../../../../moment/locale/tet.js",
	"./th": "../../../../moment/locale/th.js",
	"./th.js": "../../../../moment/locale/th.js",
	"./tl-ph": "../../../../moment/locale/tl-ph.js",
	"./tl-ph.js": "../../../../moment/locale/tl-ph.js",
	"./tlh": "../../../../moment/locale/tlh.js",
	"./tlh.js": "../../../../moment/locale/tlh.js",
	"./tr": "../../../../moment/locale/tr.js",
	"./tr.js": "../../../../moment/locale/tr.js",
	"./tzl": "../../../../moment/locale/tzl.js",
	"./tzl.js": "../../../../moment/locale/tzl.js",
	"./tzm": "../../../../moment/locale/tzm.js",
	"./tzm-latn": "../../../../moment/locale/tzm-latn.js",
	"./tzm-latn.js": "../../../../moment/locale/tzm-latn.js",
	"./tzm.js": "../../../../moment/locale/tzm.js",
	"./uk": "../../../../moment/locale/uk.js",
	"./uk.js": "../../../../moment/locale/uk.js",
	"./ur": "../../../../moment/locale/ur.js",
	"./ur.js": "../../../../moment/locale/ur.js",
	"./uz": "../../../../moment/locale/uz.js",
	"./uz-latn": "../../../../moment/locale/uz-latn.js",
	"./uz-latn.js": "../../../../moment/locale/uz-latn.js",
	"./uz.js": "../../../../moment/locale/uz.js",
	"./vi": "../../../../moment/locale/vi.js",
	"./vi.js": "../../../../moment/locale/vi.js",
	"./x-pseudo": "../../../../moment/locale/x-pseudo.js",
	"./x-pseudo.js": "../../../../moment/locale/x-pseudo.js",
	"./yo": "../../../../moment/locale/yo.js",
	"./yo.js": "../../../../moment/locale/yo.js",
	"./zh-cn": "../../../../moment/locale/zh-cn.js",
	"./zh-cn.js": "../../../../moment/locale/zh-cn.js",
	"./zh-hk": "../../../../moment/locale/zh-hk.js",
	"./zh-hk.js": "../../../../moment/locale/zh-hk.js",
	"./zh-tw": "../../../../moment/locale/zh-tw.js",
	"./zh-tw.js": "../../../../moment/locale/zh-tw.js"
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "../../../../moment/locale recursive ^\\.\\/.*$";

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map